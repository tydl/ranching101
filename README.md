# Ranching 101

## Table of Contents
* Overview
* Manual
* Obtaining the Source Code
* Contributing
* License
* Reporting Issues

## I. Overview

This game is in an alpha state, thrown together in a weekend and polished in
a day to provide a proof of concept game: manage a dairy of cowgirls and
bullboys, harvest them, and have them make love to each other.

Future ideas could include selective genetic breeding, breeder having sex with
the livestock, injury and medical management.

## II. Manual

The game attempts to guide the player with in-game commands. The basics are:

* Buy a barn.
* Fill it with a pen, a harvester, and a love pen.
* Buy livestock.
* Move livestock from pens to harvesters.
* Harvest them, their stored milk and happiness will go down. But the harvester
will store the milk.
* Move them back to their pens to avoid a large daily maintenance penalty.
* Have them make love to each other to increase their happiness.
* End the day to let them produce milk.
* Repeat.

There is no higher goal. This is in an alpha state!

## III. Obtaining the Source Code

The canonical location for the code is on
[gitlab](https://gitlab.com/tydl/ranching101), though if you are using a
different fork then it may live elsewhere.

## IV. Contributing

See the `CONTRIBUTING.md` file.

## V. License

See the accompanying COPYING file. This work is released under the GNU GPLv3
license. Share all the shit openly. So if you disagree how things are going:
fuck it and fork it.

## VI. Reporting Issues

Open an [issue](https://gitlab.com/tydl/ranching101/issues/new)
and be sure to label it as a **bug**.
