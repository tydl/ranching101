#!/bin/bash
env GOOS=windows GOARCH=386 go build -o $GOPATH/bin/ranching101_windows_386.exe -v gitlab.com/tydl/moomoo
env GOOS=windows GOARCH=amd64 go build -o $GOPATH/bin/ranching101_windows_amd64.exe -v gitlab.com/tydl/moomoo
env GOOS=darwin GOARCH=386 go build -o $GOPATH/bin/ranching101_darwin_386 -v gitlab.com/tydl/moomoo
env GOOS=darwin GOARCH=amd64 go build -o $GOPATH/bin/ranching101_darwin_amd64 -v gitlab.com/tydl/moomoo
env GOOS=linux GOARCH=386 go build -o $GOPATH/bin/ranching101_linux_386 -v gitlab.com/tydl/moomoo
env GOOS=linux GOARCH=amd64 go build -o $GOPATH/bin/ranching101_linux_amd64 -v gitlab.com/tydl/moomoo
