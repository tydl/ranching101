package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/tydl/moomoo/barn"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/market"
	"gitlab.com/tydl/moomoo/util"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	startingMoney = 30000
	menuPrompt    = "Welcome To Ranching 101\n\n" +
		"Type 'quit', 'exit', or 'q' to exit any menu, including this one.\n" +
		"Start a 'new' game or 'load <filename>' a saved game.\n\n" +
		"This is an ALPHA demo. Stuff is not balanced, complete, nor free of bugs.\n" +
		"Saves will get broken. Starts can be insane or difficult. You having\n" +
		"dirty time with the livestock is not implemented. Not even the sex pen has\n" +
		"a description for it. Also, this game will not get meta, philosophical, nor\n" +
		"psychological.\n\n" +
		"This code is GPL. That means you have the right to view, modify,\n" +
		"redistribute, and complain about this code. All of that can be done at the\n" +
		"project page at https://gitlab.com/tydl/ranching101 on the issues page."
	changeLog = "Changelog for [0.0.3]\n" +
		"\tAdded:\n" +
		"- The 'move' command will provide feedback when successful.\n" +
		"\tChanged:\n" +
		"- Barns are more expensive.\n" +
		"- Harvesters are less expensive.\n" +
		"- Pens are slightly more expensive.\n" +
		"- Livestock is generally less expensive.\n" +
		"- Maintenance penalty for livestock being left in a harvester increased\n" +
		"substantially.\n" +
		"- Maintenance costs increased for all pens.\n" +
		"\tFixed:\n" +
		"- 'save' command prompt now correctly labelled as available.\n"
)

var game *Game

func main() {
	flag.BoolVar(&util.Debug, "debug", false, "Enable debug mode")
	flag.Parse()
	menuPromptFn := func() string { return menuPrompt + "\n\n" + changeLog }
	processLoop(processMenu, menuPromptFn)
}

func initGame() {
	game = &Game{
		current: time.Date(
			2050,
			time.January,
			1,
			8,
			0,
			0,
			0,
			time.Now().Location(),
		),
		barns:             make([]barn.Barn, 0, 2),
		stock:             make(map[string]cow.Livestock),
		money:             startingMoney,
		livestockMarket:   market.NewLivestockMarket(),
		barnMarket:        market.NewBarnMarket(),
		barnSlotterMarket: market.NewBarnSlotterMarket(),
	}
}

func printBreak() {
	fmt.Println(strings.Repeat("-", 80))
}

func printCommand() {
	fmt.Println(strings.Repeat("=", 80))
}

func processLoop(processFn func(string) (bool, error), enterFunc func() string) {
	scanner := bufio.NewScanner(os.Stdin)
	if enterFunc != nil {
		if enterString := enterFunc(); len(enterString) > 0 {
			printBreak()
			fmt.Println(enterString)
		}
	}
	fmt.Print(">")
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if len(line) == 0 {
			fmt.Print(">")
			continue
		}
		quit := false
		switch strings.ToLower(line) {
		case "quit":
			fallthrough
		case "exit":
			fallthrough
		case "q":
			quit = true
		default:
		}
		if quit {
			printCommand()
			break
		}
		printCommand()
		prompt, err := processFn(line)
		if err != nil {
			printErr(err)
		} else {
			if prompt {
				if enterFunc != nil {
					if enterString := enterFunc(); len(enterString) > 0 {
						printBreak()
						fmt.Println(enterString)
					}
				}
			}
		}
		fmt.Print(">")
	}
}

func processMenu(line string) (bool, error) {
	switch split := strings.Split(line, " "); split[0] {
	case "new":
		initGame()
		processLoop(processGame, gamePrompt)
		return true, nil
	case "load":
		initGame()
		if err := handleLoad(split); err != nil {
			return false, err
		}
		processLoop(processGame, gamePrompt)
		return true, nil
	case "shitpost":
		doShitposting()
		return true, nil
	default:
		return false, errors.New("Unknown command")
	}
}

func gamePrompt() string {
	status := game.GetPromptText()
	str := fmt.Sprintf("%s\n\nAvailable commands:\n"+
		"\t'save <filename>' (Saves your game)\n"+
		"\t'status' (Overall status of barns)\n"+
		"\t'status barn <barn index>' (Status of particular barn)\n"+
		"\t'status barn <barn index> <pen index>' (Status of a particular pen)\n"+
		"\t'status livestock <livestock id>' (Status of a particular livestock)\n"+
		"\t'name' (Allows renaming - NOT SUPPORTED IN ALPHA)\n"+
		"\t'buy barn' (Enters the barn market)\n"+
		"\t'buy pen' (Enters the pen market)\n"+
		"\t'buy livestock' (Enters the livestock market)\n"+
		"\t'remove <barn index> <pen index>' (Removes an empty pen from a barn)\n"+
		"\t'move <livestock id> <to barn index> <topen index>' (Moves livestock to a specific pen)\n"+
		"\t'harvest <barn index> <harvester index>' (Harvests a livestock in the harvester)\n"+
		"\t'sell <barn index> <harvester index>' (Sells product accumulated in the harvester)\n"+
		"\t'sex <barn index> <love pen index>' (Have livestock do the dirty - DESCRIPTION NOT IN ALPHA)\n"+
		"\t'done' (End the day)",
		status)
	return str
}

func processGame(line string) (bool, error) {
	switch split := strings.Split(line, " "); strings.ToLower(split[0]) {
	case "save":
		return false, handleSave(split)
	case "status":
		return false, handleStatus(split)
	case "name":
		return false, errors.New("Not implemented in this demo")
	case "buy":
		return true, handleBuy(split)
	case "remove":
		return false, handleRemove(split)
	case "move":
		return true, handleMove(split)
	case "harvest":
		return true, handleHarvest(split)
	case "sell":
		return true, handleSell(split)
	case "sex":
		return true, handleSex(split)
	case "done":
		return true, handleDone(split)
	default:
		return false, errors.New("Unknown command")
	}
}

func handleSave(split []string) error {
	if len(split) != 2 {
		return errors.New("Please specify file to save")
	}
	bytes, err := json.Marshal(game)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(split[1], bytes, 0644)
}

func handleLoad(split []string) error {
	if len(split) != 2 {
		return errors.New("Please specify file to load")
	}
	bytes, err := ioutil.ReadFile(split[1])
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, game)

}

func handleStatus(split []string) error {
	if len(split) == 1 {
		fmt.Println(game.GetOverallStatus())
		return nil
	} else if len(split) == 2 {
		return errors.New("Specify which barn/livestock.")
	} else if len(split) == 3 {
		idx, err := getInt(split[2])
		if err != nil {
			return err
		}
		switch strings.ToLower(split[1]) {
		case "barn":
			str, err := game.GetBarnStatus(idx)
			if err != nil {
				return err
			}
			fmt.Println(str)
			return nil
		case "livestock":
			str, err := game.GetLivestockStatus(idx)
			if err != nil {
				return err
			}
			fmt.Println(str)
			return nil
		default:
			return errors.New(fmt.Sprintf("Unknown status type %s", split[1]))
		}
	} else if len(split) == 4 {
		idx, err := getInt(split[2])
		if err != nil {
			return err
		}
		slotIdx, err := getInt(split[3])
		if err != nil {
			return err
		}
		switch strings.ToLower(split[1]) {
		case "barn":
			str, err := game.GetSlotStatus(idx, slotIdx)
			if err != nil {
				return err
			}
			fmt.Println(str)
			return nil
		case "livestock":
			return errors.New("Too many arguments for 'status livestock'")
		default:
			return errors.New(fmt.Sprintf("Unknown status type %s", split[1]))
		}

	}
	return errors.New("Too many arguments to 'status'")
}

func handleBuy(split []string) error {
	if len(split) == 1 {
		return errors.New("Specify which to buy: 'barn', 'livestock', or 'pen'")
	}
	switch strings.ToLower(split[1]) {
	case "barn":
		processLoop(processBuyBarn, getBarnMarketDescription)
		return nil
	case "livestock":
		processLoop(processBuyLivestock, getLivestockMarketDescription)
		return nil
	case "pen":
		processLoop(processBuyPen, getPenMarketDescription)
		return nil
	default:
		return errors.New(fmt.Sprintf("Unknown buy type '%s'", split[1]))
	}
}

func getBarnMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s",
		game.GetBarnMarketDescription(),
		"Type the index of the barn you wish to buy, or type 'q' to quit.")
}

func getPenMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s",
		game.GetPenMarketDescription(),
		"Type the index of the pen you wish to buy. It will go into the first available\n"+
			"barn. To put it in a specific barn, also specify the barn index to put it in.\n"+
			"For example: '1 0' will buy the [1] pen and put it in the [0] barn.\n"+
			"Type 'q' to quit.")
}

func getLivestockMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s",
		game.GetLivestockMarketDescription(),
		"Type the index of the livestock you wish to buy. It will be placed into the first\n"+
			"available pen of any type. To put it in a specific barn, also specify the barn index\n"+
			"to put it in. You can also specify which pen in the barn. For example: '1 0 2' will\n"+
			"buy the [1] livestock, put it in the [0] barn and its [2] pen. Type 'q' to quit.")
}

func processBuyBarn(line string) (bool, error) {
	idx, err := getInt(line)
	if err != nil {
		return false, err
	}
	err = game.BuyBarn(idx)
	if err != nil {
		return false, err
	}
	fmt.Println("You purchased a new barn!")
	return true, nil
}

func processBuyLivestock(line string) (bool, error) {
	split := strings.Split(line, " ")
	if len(split) == 1 {
		marketIdx, err := getInt(split[0])
		if err != nil {
			return false, err
		}
		err = game.BuyLivestock(marketIdx, -1, -1)
		if err != nil {
			return false, err
		}
	} else if len(split) == 2 {
		marketIdx, err := getInt(split[0])
		if err != nil {
			return false, err
		}
		barnIdx, err := getInt(split[1])
		if err != nil {
			return false, err
		}
		err = game.BuyLivestock(marketIdx, barnIdx, -1)
		if err != nil {
			return false, err
		}
	} else if len(split) != 3 {
		return false, errors.New("Specify index of livestock, then barn index, then slot index")
	} else {
		marketIdx, err := getInt(split[0])
		if err != nil {
			return false, err
		}
		barnIdx, err := getInt(split[1])
		if err != nil {
			return false, err
		}
		slotIdx, err := getInt(split[2])
		if err != nil {
			return false, err
		}
		err = game.BuyLivestock(marketIdx, barnIdx, slotIdx)
		if err != nil {
			return false, err
		}
	}
	fmt.Println("You purchased new livestock!")
	return true, nil
}

func processBuyPen(line string) (bool, error) {
	split := strings.Split(line, " ")
	if len(split) == 1 {
		marketIdx, err := getInt(split[0])
		if err != nil {
			return false, err
		}
		err = game.BuyBarnSlotter(marketIdx, -1)
		if err != nil {
			return false, err
		}
	} else if len(split) != 2 {
		return false, errors.New("Specify index of barn")
	} else {
		marketIdx, err := getInt(split[0])
		if err != nil {
			return false, err
		}
		barnIdx, err := getInt(split[1])
		if err != nil {
			return false, err
		}
		err = game.BuyBarnSlotter(marketIdx, barnIdx)
		if err != nil {
			return false, err
		}
	}
	fmt.Println("You purchased a new pen!")
	return true, nil
}

func handleRemove(split []string) error {
	if len(split) != 3 {
		return errors.New("'remove' needs barn and pen ID")
	}
	barnIdx, err := getInt(split[1])
	if err != nil {
		return err
	}
	penIdx, err := getInt(split[2])
	if err != nil {
		return err
	}
	return game.RemoveBarnSlotter(barnIdx, penIdx)
}

func handleMove(split []string) error {
	if len(split) != 4 {
		return errors.New("'move' needs livestock ID and barn and pen ID it is moooooving to")
	}
	toBarnIdx, err := getInt(split[2])
	if err != nil {
		return err
	}
	toPenIdx, err := getInt(split[3])
	if err != nil {
		return err
	}
	return game.MoveLivestock(split[1], toBarnIdx, toPenIdx)
}

func handleHarvest(split []string) error {
	if len(split) != 3 {
		return errors.New("'harvest' barn and pen ID")
	}
	barnIdx, err := getInt(split[1])
	if err != nil {
		return err
	}
	penIdx, err := getInt(split[2])
	if err != nil {
		return err
	}
	desc, err, errs := game.Harvest(barnIdx, penIdx)
	if err != nil {
		return err
	} else if len(errs) > 0 {
		for i := 1; i < len(errs); i++ {
			printErr(errs[i])
		}
		return errs[0]
	} else {
		fmt.Println(desc)
	}
	return nil
}

func handleSell(split []string) error {
	if len(split) != 3 {
		return errors.New("'sell' barn and pen ID")
	}
	barnIdx, err := getInt(split[1])
	if err != nil {
		return err
	}
	penIdx, err := getInt(split[2])
	if err != nil {
		return err
	}
	gain, err := game.Sell(barnIdx, penIdx)
	if err != nil {
		return err
	} else {
		fmt.Printf("You made $%d!\n", gain)
	}
	return nil
}

func handleSex(split []string) error {
	if len(split) != 3 {
		return errors.New("'sex' barn and pen ID")
	}
	barnIdx, err := getInt(split[1])
	if err != nil {
		return err
	}
	penIdx, err := getInt(split[2])
	if err != nil {
		return err
	}
	desc, err, errs := game.MakeLove(barnIdx, penIdx)
	if err != nil {
		return err
	} else if len(errs) > 0 {
		for i := 1; i < len(errs); i++ {
			printErr(errs[i])
		}
		return errs[0]
	} else {
		fmt.Println(desc)
	}
	return nil
}

func handleDone(split []string) error {
	lose, str := game.PassDay()
	if lose {
		fmt.Println("You don't have enough money to pay your maintenance costs. A grue emerges from the shadows and eats you. You lose! :(")
		os.Exit(0)
		return nil
	}
	fmt.Println(str)
	return nil
}

func printErr(err error) {
	fmt.Printf("Error: %s\n", err)
}

func getInt(str string) (int, error) {
	idx, err := strconv.Atoi(str)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("%s is not an integer", str))
	}
	return idx, nil
}

func doShitposting() {
	shitposts := []string{
		"CoC isn't furry",
		"CoC is furry",
		"This is some impressive autism.",
		"nice meme",
		">\"nu-male\"",
		"REEEEEEEEEEEEEEEEEEEEEEEEE",
		"What is FC about? sounds interersting",
		"Yeah but Korpiklaani.",
		"I care so little about the garbage you're offering that I can't even be bothered to logic-check my posts. also, fuck off, we don't need any more ideas guys",
		"Alright I'm on it. The game is now about Hogwarts. The name is no longer Jack Tales but something cool I dunno yet fuck off.",
		"Nobody said you don't seem excited, consider learning English.",
		"And, I mean, other than from certain ethical standpoints, it's hard to say Fenoxo's doing it wrong - it's like all the other bullshit corporate america does.",
		"What's wrong with Leddit? It worked for this guy's game :^)",
		"Because rape isn't cool.",
		"4chan gets triggered by dumber shit than tumblr sometimes.",
		"REEEEEEEEEEEEE",
		"Not a hugbox my hairy testicles.",
		"Has there even been a /dgg/ thread that didn't devolve into shitposting at or around 150 posts? It's like clockwork at this point.",
		"wew thanks lad",
		"Hey, you better start respecting my safe space before I call the mods on you.",
		"This is NOT FC general",
		"People like you are basically furries.",
		"(Replying to picture of Motorhead shirt) Brutal Legend shirt, nice!",
		"Everyone, please stop replying to the clearly autistic guy.",
		"My life is like a porn game. Poorly made and there's no end goal but at least there is some perverted shit to fap to, so I guess it's okay.",
		">It's just one more step. What's the big deal?",
		"\"This one single thing at TL8 exists, so why not add another one at TL7? And another one. And one at TL8. We already have something that advanced, after all. In fact, why not add everything from TL7? We have two things at TL8, you know. It would be inconsistent NOT to have everything from TL7! And since we have two things at TL8, why not add another one? Also, we now have three things at TL8, so what's the big deal about adding one little thing from TL9?\"",
		"I found this post rather shallow and pedantic",
		"Post a changelog you faggot.",
		"also, I don't know why that greentext appeared in my post, but that is only more evidence for the incompetence theory",
		"I followed the link to TFGamesSite, but the only links to the game I found were rar files and I don't understand why or how I would use one. I have tried opening said files in chrome, but it just gives me more of that rar file",
		"Well, since you want to get pedantic about it, 8 orgasms per minute are literally impossible.",
	}
	now := time.Now()
	rng := rand.New(rand.NewSource(int64(now.Second())))
	var maxSleep int64 = 1500
	var minSleep int64 = 250
	for i := 0; i < 10; i++ {
		idx := rng.Intn(len(shitposts))
		fmt.Println(shitposts[idx])
		sleep := rng.Int63n(maxSleep - minSleep)
		time.Sleep(time.Duration(sleep+minSleep) * time.Millisecond)
	}
}
