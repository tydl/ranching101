# Contributing

## Learn Go

It is a very small language, so it should be easily accessible to those with
little or no programming knowledge. Start with the
[tour](https://tour.golang.org).

## Learn Git

Git is a version control system, archiving the history of the code. There are a
ton of tutorials of various quality and different approaches. Pick one, if it
is not helpful then bail and pick another one. Git can be complex, but for this
project you will only need the basic commands and concepts.

## Register on Gitlab

They host the remote repository, so register on the site.

## Explore and File Issues

Go to the repository and read up on issues. Open new issues if needed. Be sure
to select an appropriate label.

## Collaborate, Code and Contribute

Pull the repo, discuss solutions, code them up, send a merge request.
