# Change Log

This file contains details for each release.

## Unreleased
### Added

- Easter egg command at the main menu to help preserve the glory that is /dgg/.

### Changed

- Price for milk/semen increased from 10 to 15.

### Fixed

- Problems buying at the market should not eat up money.

## [0.0.3] - 2016-09-01
### Added

- The 'move' command will provide feedback when successful.

### Changed

- Barns are more expensive.
- Harvesters are less expensive.
- Pens are slightly more expensive.
- Livestock is generally less expensive.
- Maintenance penalty for livestock being left in a harvester increased
substantially.
- Maintenance costs increased for all pens.

### Fixed

- 'save' command prompt now correctly labelled as available.

## [0.0.2] - 2016-08-29
### Added

- Games can now be saved and loaded.
- Futa cows have harvest text.
- Placeholder text for love pen masturbation and sex scenes. They should
properly detect how many and what combination of cows, bulls, and futa cows are
contained in the pen.

### Changed

- Love Pens cost raised (should no longer have negative price).

### Fixed

- Minor text fixes.
- Love Pens pricing should no longer be negative (raised base cost).

## [0.0.1] - 2016-08-27
### Added

- Futas can be bought and harvested (they do not have harvest text).
- Bulls can be bought and harvested.
- Bulls have harvest text.

### Changed

- Buying commands in market can put pens and livestock in first available
space.

### Fixed

- Cows will now properly refresh after being harvested, so they can be
harvested once again.

## [0.0.0] - 2016-08-22
### Added

- Initial alpha of the game.
- Ability to create new games, but not save nor load.
- Can buy farms, pens, and cows.
- Can harvest livestock and move them from pen to pen.
- Can sell accumulated milk.
- Can trigger end of day.
