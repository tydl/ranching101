package genes

import (
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"math/rand"
)

var _ vow.GeneFactory = &randomGeneFactory{}

type WeightedGenes struct {
	Weight   int
	Genotype Genotype
}

// TODO: Create joint probability distributions?
type randomGeneFactory struct {
	genesForSlot map[int][]WeightedGenes
	rng          *rand.Rand
}

func (r *randomGeneFactory) GetGenotype(slot int) (Genotype, error) {
	available, ok := r.genesForSlot[slot]
	if !ok {
		return nil, errors.New(fmt.Sprintf("No gene for slot %d", slot))
	}
	totalWeight := 0
	for _, weightedGenes := range available {
		totalWeight += weightedGenes.Weight
	}
	geneIdx := r.rng.Intn(totalWeight)
	weight := 0
	for _, weightedGenes := range available {
		weight += weightedGenes.Weight
		if geneIdx < weight {
			return weightedGenes.Genotype, nil
		}
	}
	return nil, errors.New(fmt.Sprintf("When determining gene, got index of %d out of %d", geneIdx, totalWeight))
}
