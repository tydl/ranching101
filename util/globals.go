package util

import (
	"bytes"
	"fmt"
	"math"
	"math/rand"
	"time"
)

var id int = 0

func getId() int {
	toRet := id
	id++
	return toRet
}

func GetIdString() string {
	return fmt.Sprintf("%d", getId())
}

func UniformBetween(r *rand.Rand, min, max int) int {
	return r.Intn(max-min+1) + min
}

func UniformBetweenDurations(r *rand.Rand, min, max time.Duration) time.Duration {
	return time.Duration(r.Int63n(int64(max)-int64(min)+1) + int64(min))
}

func UniformMultiplier(r *rand.Rand, tenthsPercent int) float64 {
	randTenthsPercent := r.Intn(tenthsPercent*2 + 1)
	randTenthsPercent += 1000 - tenthsPercent
	return float64(randTenthsPercent) / 1000
}

func MultToCost(mult float64, percentRateCost int) int {
	percentDiff := (mult - 1) * 100
	return int(math.Floor(percentDiff*float64(percentRateCost) + 0.5))
}

func HappinessToString(i int) string {
	if i < 25 {
		return "Grumpy"
	} else if i < 50 {
		return "Unhappy"
	} else if i < 75 {
		return "Content"
	} else if i < 100 {
		return "Happy"
	} else if i < 125 {
		return "Joyful"
	} else {
		return "Ecstatic"
	}
}

func SemenFullnessToString(curr, max int) string {
	frac := float64(curr) / float64(max)
	if frac == 0 {
		return "Empty"
	} else if frac < 0.1 {
		return "Very Low"
	} else if frac < 0.25 {
		return "Low"
	} else if frac < 0.5 {
		return "Some"
	} else if frac < 0.75 {
		return "Enough"
	} else if frac < 0.9 {
		return "Nearly Full"
	} else {
		return "Swollen Full"
	}
}

func MilkFullnessToString(curr, max int) string {
	frac := float64(curr) / float64(max)
	if frac == 0 {
		return "Dry"
	} else if frac < 0.1 {
		return "Very Low"
	} else if frac < 0.25 {
		return "Low"
	} else if frac < 0.5 {
		return "Some"
	} else if frac < 0.75 {
		return "Enough"
	} else if frac < 0.9 {
		return "Nearly Full"
	} else {
		return "Full"
	}
}

func FormatDuration(d time.Duration) string {
	h := int(math.Floor(d.Hours()))
	m := int(math.Floor(d.Minutes())) - (h * 60)
	if h == 0 {
		return fmt.Sprintf("%dm", m)
	}
	return fmt.Sprintf("%dh %dm", h, m)
}

func WordWrap(s string) string {
	var toReturn bytes.Buffer
	lastNewline := 0
	spaceIdx := 0
	nextSpaceIdx := 0
	for i := 0; i < len(s); i++ {
		if s[i] == ' ' {
			if spaceIdx == nextSpaceIdx {
				nextSpaceIdx = i
			} else {
				toReturn.WriteString(s[spaceIdx:nextSpaceIdx])
				spaceIdx = nextSpaceIdx
				nextSpaceIdx = i
			}
		} else if s[i] == '\n' {
			toReturn.WriteString(s[spaceIdx:i])
			spaceIdx = i + 1
			nextSpaceIdx = i + 1
			lastNewline = i
		}
		if i-lastNewline > 80 {
			toReturn.WriteString(s[spaceIdx:nextSpaceIdx])
			toReturn.WriteString("\n")
			spaceIdx = nextSpaceIdx + 1
			nextSpaceIdx = spaceIdx
			lastNewline = i - 1
		}
	}
	toReturn.WriteString(s[spaceIdx:])
	return toReturn.String()
}
