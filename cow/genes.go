package cow

type SkinColor string

const (
	White    SkinColor = "white"
	Asian    SkinColor = "asian"
	Hispanic SkinColor = "hispanic"
	Ebony    SkinColor = "ebony"
)

type EyeColor string

const (
	Brown  EyeColor = "brown"
	Blue   EyeColor = "blue"
	Hazel  EyeColor = "hazel"
	Green  EyeColor = "green"
	Orange EyeColor = "orange"
	Red    EyeColor = "red"
)

type HairColor string

const (
	BrownHair  HairColor = "brown"
	BlueHair   HairColor = "blue"
	BlondeHair HairColor = "blonde"
	GreenHair  HairColor = "green"
	BlackHair  HairColor = "black"
	RedHair    HairColor = "red"
)

type Phenotype struct {
	AdditionalOppositeGenetalia bool
	DoubleGenetalia             bool
	Virile                      bool
	HairColor                   HairColor
	LeftEyeColor                EyeColor
	RightEyeColor               EyeColor
	SkinColor                   SkinColor
	Freckles                    bool
}

type Genotype struct {
	Slot      int
	Dominance int
	Phenotype
}

type GeneFactory interface {
	GetGenotype(slot int) (Genotype, error)
}
