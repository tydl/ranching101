package cow

import (
	"encoding/json"
	"time"
)

type InjuryType string

const (
	None      InjuryType = "None"
	Nipples   InjuryType = "Nipple Tenderness"
	Bruises   InjuryType = "Bruises"
	Glands    InjuryType = "Gland Aches"
	Infection InjuryType = "Infection"
)

type Statuser interface {
	Status() string
	MarketStatus() string
}

type Lovable interface {
	MakeLove(
		timeMultiplier float64,
		happinessMultiplier float64,
		growthMultiplier float64,
	) time.Duration
}

type Harvestable interface {
	Harvest(
		productMultiplier float64,
		timeMultiplier float64,
		happinessMultiplier float64,
		injuryMultiplier float64,
		growthMultiplier float64,
		recoveryMultiplier float64,
	) (product int, dur time.Duration, description string, err error)
}

type Livestock interface {
	Statuser
	Id() string
	SetName(string)
	Name() string
	Cost() int
	Lovable
	Harvestable
	Happiness() (current, max int)
	InjuryStatus() InjuryType
	Pass(time.Duration)
	json.Marshaler
}

type Milkable interface {
	MilkStored() (current, max int)
}

type Semener interface {
	SemenStored() (current, max int)
}
