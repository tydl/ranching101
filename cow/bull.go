package cow

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/util"
	"math"
	"time"
)

const (
	costPerMaxSemen = 30
)

var _ Livestock = &basicBull{}
var _ Semener = &basicBull{}

type basicBull struct {
	Ident                string
	BullName             string
	MaxHappiness         int
	CurrHappiness        int
	Injury               InjuryType
	MaxSemen             int
	Semen                int
	BaseHarvestTime      time.Duration
	BaseLoveTime         time.Duration
	BaseInjuryRate       float64
	BaseHappinessHarvest int
	BaseHappinessLove    int
	BaseRecoveryTime     time.Duration
	BaseGrowthRate       int
	DurationPerUnit      time.Duration
	TimeToRecover        time.Duration
}

func (b *basicBull) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicBull) Id() string {
	return b.Ident
}

func (b *basicBull) Name() string {
	return b.BullName
}

func (b *basicBull) SetName(s string) {
	b.BullName = s
}

func (b *basicBull) Happiness() (current, max int) {
	return b.CurrHappiness, b.MaxHappiness
}

func (b *basicBull) InjuryStatus() InjuryType {
	return b.Injury
}

func (b *basicBull) Cost() int {
	return (b.MaxHappiness-minMaxHappiness)*costPerMaxHappiness +
		(b.MaxSemen-minMaxMilk)*costPerMaxSemen +
		int((maxBaseHarvestTime-b.BaseHarvestTime)/time.Minute)*costPerBaseMinuteHarvestTime +
		int((maxBaseLoveTime-b.BaseLoveTime)/time.Minute)*costPerBaseMinuteLoveTime +
		int((maxBaseRecoveryTime-b.BaseRecoveryTime)/time.Hour)*costPerBaseRecoveryTime +
		b.BaseGrowthRate*costPerBaseGrowthRate +
		int((maxDurationPerUnit-b.DurationPerUnit)/time.Minute)*costPerDurationPerUnit
}

func (b *basicBull) Pass(t time.Duration) {
	if util.Debug {
		fmt.Println("Debug: basicBull.Pass")
		fmt.Printf("t=%s\n", t)
	}
	passed := t - b.TimeToRecover
	b.TimeToRecover -= t
	if passed <= 0 || t == 0 {
		if util.Debug {
			fmt.Println("Debug: basicBull.Pass reduce time to recover")
			fmt.Printf("passed=%s\nb.TimeToRecover=%s\n", passed, b.TimeToRecover)
		}
		b.TimeToRecover -= t
		return
	} else if b.DurationPerUnit == 0 {
		b.Semen = b.MaxSemen
		return
	}
	gain := int(t / b.DurationPerUnit)
	if util.Debug {
		fmt.Println("Debug: basicBull.Pass increasing Semen")
		fmt.Printf("passed=%s\nSemen to add=%d\n", passed, gain)
	}

	b.Semen += gain
	if b.Semen > b.MaxSemen {
		b.Semen = b.MaxSemen
	}
}

func (b *basicBull) Harvest(
	productMultiplier float64,
	timeMultiplier float64,
	happinessMultiplier float64,
	injuryMultiplier float64,
	growthMultiplier float64,
	recoveryMultiplier float64,
) (product int, dur time.Duration, desc string, err error) {
	if b.TimeToRecover > 0 {
		if util.Debug {
			fmt.Println("Debug: basicBull.Harvest time to recover > 0")
			fmt.Printf("b.TimeToRecover=%s\n", b.TimeToRecover)
		}
		err = errors.New(fmt.Sprintf("%s needs %s to recover", b.BullName, util.FormatDuration(b.TimeToRecover)))
		return
	}
	product = int(math.Floor(float64(b.Semen)*productMultiplier + 0.5))
	dur = time.Duration(math.Floor(float64(b.BaseHarvestTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessHarvest)*happinessMultiplier + 0.5))
	b.MaxSemen += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Semen = 0
	b.TimeToRecover = time.Duration(math.Floor(float64(b.BaseRecoveryTime)*recoveryMultiplier+0.5)) + dur
	// TODO: Later: Injury
	desc = getHarvestBullText(b, product)
	if util.Debug {
		fmt.Println("Debug: basicBull.Harvest")
		fmt.Printf("product=%d\ndur=%s\nb.CurrHappiness=%d\nb.MaxSemen=%d\nb.timeToRevover=%s\n", product, dur, b.CurrHappiness, b.MaxSemen, b.TimeToRecover)
	}
	return
}

func (b *basicBull) MakeLove(
	timeMultiplier float64,
	happinessMultiplier float64,
	growthMultiplier float64,
) time.Duration {
	dur := time.Duration(math.Floor(float64(b.BaseLoveTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessLove)*happinessMultiplier + 0.5))
	b.MaxSemen += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Semen = 0
	return dur
}

func (b *basicBull) SemenStored() (current, max int) {
	return b.Semen, b.MaxSemen
}

func (b *basicBull) Status() string {
	happiness, _ := b.Happiness()
	happinessString := util.HappinessToString(happiness)
	current, max := b.SemenStored()
	semenString := util.SemenFullnessToString(current, max)
	str := fmt.Sprintf("[%s] %s:\n\tHappiness: %s\n\tSemen Stored: %s\n\tInjury: %s\n%s", b.Id(), b.Name(), happinessString, semenString, b.InjuryStatus(), b.getStatsString())
	return str
}

func (b *basicBull) MarketStatus() string {
	str := fmt.Sprintf("%s\n%s", b.Name(), b.getStatsString())
	return str
}

func (b *basicBull) getStatsString() string {
	return fmt.Sprintf("\t%s\n\t%s\n\tBase Harvest Time: %s\n\tBase Lovemaking Time: %s\n\tBase Recovery Time: %s\n\tTime To Make 1 Semen: %s",
		maxSemenString(b.MaxSemen),
		maxHappinessString(b.MaxHappiness),
		util.FormatDuration(b.BaseHarvestTime),
		util.FormatDuration(b.BaseLoveTime),
		util.FormatDuration(b.BaseRecoveryTime),
		util.FormatDuration(b.DurationPerUnit))
}

func maxSemenString(i int) string {
	if i < 50 {
		return "Petite Balls"
	} else if i < 75 {
		return "Small Balls"
	} else if i < 125 {
		return "Average Balls"
	} else if i < 175 {
		return "Large Balls"
	}
	return "Massive Balls"
}
