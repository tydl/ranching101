package cow

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/util"
	"math"
	"time"
)

var _ Livestock = &basicFuta{}
var _ Milkable = &basicFuta{}
var _ Semener = &basicFuta{}

type basicFuta struct {
	Ident                string
	FutaName             string
	MaxHappiness         int
	CurrHappiness        int
	Injury               InjuryType
	MaxMilk              int
	Milk                 int
	MaxSemen             int
	Semen                int
	BaseHarvestTime      time.Duration
	BaseLoveTime         time.Duration
	BaseInjuryRate       float64
	BaseHappinessHarvest int
	BaseHappinessLove    int
	BaseRecoveryTime     time.Duration
	BaseGrowthRate       int
	DurationPerUnit      time.Duration
	TimeToRecover        time.Duration
}

func (b *basicFuta) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicFuta) Id() string {
	return b.Ident
}

func (b *basicFuta) Name() string {
	return b.FutaName
}

func (b *basicFuta) SetName(s string) {
	b.FutaName = s
}

func (b *basicFuta) Happiness() (current, max int) {
	return b.CurrHappiness, b.MaxHappiness
}

func (b *basicFuta) InjuryStatus() InjuryType {
	return b.Injury
}

func (b *basicFuta) Cost() int {
	return (b.MaxHappiness-minMaxHappiness)*costPerMaxHappiness +
		(b.MaxMilk-minMaxMilk)*costPerMaxMilk +
		(b.MaxSemen-minMaxMilk)*costPerMaxSemen +
		int((maxBaseHarvestTime-b.BaseHarvestTime)/time.Minute)*costPerBaseMinuteHarvestTime +
		int((maxBaseLoveTime-b.BaseLoveTime)/time.Minute)*costPerBaseMinuteLoveTime +
		int((maxBaseRecoveryTime-b.BaseRecoveryTime)/time.Hour)*costPerBaseRecoveryTime +
		b.BaseGrowthRate*costPerBaseGrowthRate +
		int((maxDurationPerUnit-b.DurationPerUnit)/time.Minute)*costPerDurationPerUnit
}

func (b *basicFuta) Pass(t time.Duration) {
	if util.Debug {
		fmt.Println("Debug: basicFuta.Pass")
		fmt.Printf("t=%s\n", t)
	}
	passed := t - b.TimeToRecover
	b.TimeToRecover -= t
	if passed <= 0 || t == 0 {
		if util.Debug {
			fmt.Println("Debug: basicFuta.Pass reduce time to recover")
			fmt.Printf("passed=%s\nb.TimeToRecover=%s\n", passed, b.TimeToRecover)
		}
		b.TimeToRecover -= t
		return
	} else if b.DurationPerUnit == 0 {
		b.Milk = b.MaxMilk
		b.Semen = b.MaxSemen
		return
	}
	gain := int(t / b.DurationPerUnit)
	if util.Debug {
		fmt.Println("Debug: basicFuta.Pass increasing Milk")
		fmt.Printf("passed=%s\nMilk to add=%d\n", passed, gain)
	}

	b.Milk += gain
	b.Semen += gain
	if b.Milk > b.MaxMilk {
		b.Milk = b.MaxMilk
	}
	if b.Semen > b.MaxSemen {
		b.Semen = b.MaxSemen
	}
}

func (b *basicFuta) Harvest(
	productMultiplier float64,
	timeMultiplier float64,
	HappinessMultiplier float64,
	InjuryMultiplier float64,
	growthMultiplier float64,
	recoveryMultiplier float64,
) (product int, dur time.Duration, desc string, err error) {
	if b.TimeToRecover > 0 {
		if util.Debug {
			fmt.Println("Debug: basicFuta.Harvest time to recover > 0")
			fmt.Printf("b.TimeToRecover=%s\n", b.TimeToRecover)
		}
		err = errors.New(fmt.Sprintf("%s needs %s to recover", b.FutaName, util.FormatDuration(b.TimeToRecover)))
		return
	}
	Milk := int(math.Floor(float64(b.Milk)*productMultiplier + 0.5))
	Semen := int(math.Floor(float64(b.Semen)*productMultiplier + 0.5))
	product = Milk + Semen
	dur = time.Duration(math.Floor(float64(b.BaseHarvestTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessHarvest)*HappinessMultiplier + 0.5))
	b.MaxMilk += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.MaxSemen += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Milk = 0
	b.Semen = 0
	b.TimeToRecover = time.Duration(math.Floor(float64(b.BaseRecoveryTime)*recoveryMultiplier+0.5)) + dur
	// TODO: Later: Injury
	desc = getHarvestFutaText(b, Milk, Semen)
	if util.Debug {
		fmt.Println("Debug: basicFuta.Harvest")
		fmt.Printf("product=%d\ndur=%s\nb.CurrHappiness=%d\nb.MaxMilk=%d\nb.MaxSemen=%d\nb.timeToRevover=%s\n", product, dur, b.CurrHappiness, b.MaxMilk, b.MaxSemen, b.TimeToRecover)
	}
	return
}

func (b *basicFuta) MakeLove(
	timeMultiplier float64,
	HappinessMultiplier float64,
	growthMultiplier float64,
) time.Duration {
	dur := time.Duration(math.Floor(float64(b.BaseLoveTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessLove)*HappinessMultiplier + 0.5))
	b.MaxMilk += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.MaxSemen += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Milk = 0
	b.Semen = 0
	return dur
}

func (b *basicFuta) MilkStored() (current, max int) {
	return b.Milk, b.MaxMilk
}

func (b *basicFuta) SemenStored() (current, max int) {
	return b.Semen, b.MaxSemen
}

func (b *basicFuta) Status() string {
	happiness, _ := b.Happiness()
	happinessString := util.HappinessToString(happiness)
	current, max := b.MilkStored()
	milkString := util.MilkFullnessToString(current, max)
	semenString := util.SemenFullnessToString(b.SemenStored())
	str := fmt.Sprintf("[%s] %s:\n\tHappiness: %s\n\tMilk Stored: %s\n\tSemen Stored: %s\n\tInjury: %s\n%s", b.Id(), b.Name(), happinessString, milkString, semenString, b.InjuryStatus(), b.getStatsString())
	return str
}

func (b *basicFuta) MarketStatus() string {
	str := fmt.Sprintf("%s\n%s", b.Name(), b.getStatsString())
	return str
}

func (b *basicFuta) getStatsString() string {
	return fmt.Sprintf("\t%s\n\t%s\n\t%s\n\tBase Harvest Time: %s\n\tBase Lovemaking Time: %s\n\tBase Recovery Time: %s\n\tTime To Make 1 Milk: %s",
		maxMilkString(b.MaxMilk),
		maxSemenString(b.MaxSemen),
		maxHappinessString(b.MaxHappiness),
		util.FormatDuration(b.BaseHarvestTime),
		util.FormatDuration(b.BaseLoveTime),
		util.FormatDuration(b.BaseRecoveryTime),
		util.FormatDuration(b.DurationPerUnit))
}

func LoadStock(stock json.RawMessage) (map[string]Livestock, error) {
	m := make(map[string]json.RawMessage, 0)
	err := json.Unmarshal(stock, &m)
	if err != nil {
		return nil, err
	}
	toRet := make(map[string]Livestock, len(m))
	for _, raw := range m {
		l, err := UnmarshalLivestock(raw)
		if err != nil {
			return nil, err
		}
		toRet[l.Id()] = l
	}
	return toRet, nil
}

func UnmarshalLivestock(raw json.RawMessage) (Livestock, error) {
	var futa basicFuta
	var bull basicBull
	var cow basicCow
	fmt.Printf("UnmarshalLivestock %s\n", string(raw))
	err := json.Unmarshal([]byte(raw), &futa)
	if err != nil {
		return nil, err
	}
	if futa.MaxMilk > 0 && futa.MaxSemen > 0 {
		return &futa, nil
	}
	err = json.Unmarshal([]byte(raw), &bull)
	if err != nil {
		return nil, err
	}
	if bull.MaxSemen > 0 {
		return &bull, nil
	}
	err = json.Unmarshal([]byte(raw), &cow)
	if err != nil {
		return nil, err
	}
	return &cow, nil
}
