package cow

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/util"
	"math"
	"time"
)

const (
	costPerMaxHappiness          = 3
	costPerMaxMilk               = 30
	costPerBaseMinuteHarvestTime = 10
	costPerBaseMinuteLoveTime    = 10
	costPerBaseRecoveryTime      = 125
	costPerBaseGrowthRate        = 200
	costPerDurationPerUnit       = 75
)

var _ Livestock = &basicCow{}
var _ Milkable = &basicCow{}

type basicCow struct {
	Ident                string
	CowName              string
	MaxHappiness         int
	CurrHappiness        int
	Injury               InjuryType
	MaxMilk              int
	Milk                 int
	BaseHarvestTime      time.Duration
	BaseLoveTime         time.Duration
	BaseInjuryRate       float64
	BaseHappinessHarvest int
	BaseHappinessLove    int
	BaseRecoveryTime     time.Duration
	BaseGrowthRate       int
	DurationPerUnit      time.Duration
	TimeToRecover        time.Duration
}

func (b *basicCow) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicCow) Id() string {
	return b.Ident
}

func (b *basicCow) Name() string {
	return b.CowName
}

func (b *basicCow) SetName(s string) {
	b.CowName = s
}

func (b *basicCow) Happiness() (current, max int) {
	return b.CurrHappiness, b.MaxHappiness
}

func (b *basicCow) InjuryStatus() InjuryType {
	return b.Injury
}

func (b *basicCow) Cost() int {
	return (b.MaxHappiness-minMaxHappiness)*costPerMaxHappiness +
		(b.MaxMilk-minMaxMilk)*costPerMaxMilk +
		int((maxBaseHarvestTime-b.BaseHarvestTime)/time.Minute)*costPerBaseMinuteHarvestTime +
		int((maxBaseLoveTime-b.BaseLoveTime)/time.Minute)*costPerBaseMinuteLoveTime +
		int((maxBaseRecoveryTime-b.BaseRecoveryTime)/time.Hour)*costPerBaseRecoveryTime +
		b.BaseGrowthRate*costPerBaseGrowthRate +
		int((maxDurationPerUnit-b.DurationPerUnit)/time.Minute)*costPerDurationPerUnit
}

func (b *basicCow) Pass(t time.Duration) {
	if util.Debug {
		fmt.Println("Debug: basicCow.Pass")
		fmt.Printf("t=%s\n", t)
	}
	passed := t - b.TimeToRecover
	b.TimeToRecover -= t
	if passed <= 0 || t == 0 {
		if util.Debug {
			fmt.Println("Debug: basicCow.Pass reduce time to recover")
			fmt.Printf("passed=%s\nb.TimeToRecover=%s\n", passed, b.TimeToRecover)
		}
		b.TimeToRecover -= t
		return
	} else if b.DurationPerUnit == 0 {
		b.Milk = b.MaxMilk
		return
	}
	gain := int(t / b.DurationPerUnit)
	if util.Debug {
		fmt.Println("Debug: basicCow.Pass increasing Milk")
		fmt.Printf("passed=%s\nMilk to add=%d\n", passed, gain)
	}

	b.Milk += gain
	if b.Milk > b.MaxMilk {
		b.Milk = b.MaxMilk
	}
}

func (b *basicCow) Harvest(
	productMultiplier float64,
	timeMultiplier float64,
	happinessMultiplier float64,
	injuryMultiplier float64,
	growthMultiplier float64,
	recoveryMultiplier float64,
) (product int, dur time.Duration, desc string, err error) {
	if b.TimeToRecover > 0 {
		if util.Debug {
			fmt.Println("Debug: basicCow.Harvest time to recover > 0")
			fmt.Printf("b.TimeToRecover=%s\n", b.TimeToRecover)
		}
		err = errors.New(fmt.Sprintf("%s needs %s to recover", b.CowName, util.FormatDuration(b.TimeToRecover)))
		return
	}
	product = int(math.Floor(float64(b.Milk)*productMultiplier + 0.5))
	dur = time.Duration(math.Floor(float64(b.BaseHarvestTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessHarvest)*happinessMultiplier + 0.5))
	b.MaxMilk += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Milk = 0
	b.TimeToRecover = time.Duration(math.Floor(float64(b.BaseRecoveryTime)*recoveryMultiplier+0.5)) + dur
	// TODO: Later: Injury
	desc = getHarvestCowText(b, product)
	if util.Debug {
		fmt.Println("Debug: basicCow.Harvest")
		fmt.Printf("product=%d\ndur=%s\nb.CurrHappiness=%d\nb.MaxMilk=%d\nb.timeToRevover=%s\n", product, dur, b.CurrHappiness, b.MaxMilk, b.TimeToRecover)
	}
	return
}

func (b *basicCow) MakeLove(
	timeMultiplier float64,
	HappinessMultiplier float64,
	growthMultiplier float64,
) time.Duration {
	dur := time.Duration(math.Floor(float64(b.BaseLoveTime)*timeMultiplier + 0.5))
	b.CurrHappiness += int(math.Floor(float64(b.BaseHappinessLove)*HappinessMultiplier + 0.5))
	b.MaxMilk += int(math.Floor(float64(b.BaseGrowthRate)*growthMultiplier + 0.5))
	b.Milk = 0
	return dur
}

func (b *basicCow) MilkStored() (current, max int) {
	return b.Milk, b.MaxMilk
}

func (b *basicCow) Status() string {
	happiness, _ := b.Happiness()
	happinessString := util.HappinessToString(happiness)
	current, max := b.MilkStored()
	milkString := util.MilkFullnessToString(current, max)
	str := fmt.Sprintf("[%s] %s:\n\tHappiness: %s\n\tMilk Stored: %s\n\tInjury: %s\n%s", b.Id(), b.Name(), happinessString, milkString, b.InjuryStatus(), b.getStatsString())
	return str
}

func (b *basicCow) MarketStatus() string {
	str := fmt.Sprintf("%s\n%s", b.Name(), b.getStatsString())
	return str
}

func (b *basicCow) getStatsString() string {
	return fmt.Sprintf("\t%s\n\t%s\n\tBase Harvest Time: %s\n\tBase Lovemaking Time: %s\n\tBase Recovery Time: %s\n\tTime To Make 1 Milk: %s",
		maxMilkString(b.MaxMilk),
		maxHappinessString(b.MaxHappiness),
		util.FormatDuration(b.BaseHarvestTime),
		util.FormatDuration(b.BaseLoveTime),
		util.FormatDuration(b.BaseRecoveryTime),
		util.FormatDuration(b.DurationPerUnit))
}

func maxMilkString(i int) string {
	if i < 50 {
		return "Petite Tits"
	} else if i < 75 {
		return "Small Tits"
	} else if i < 125 {
		return "Average Tits"
	} else if i < 175 {
		return "Large Tits"
	}
	return "Massive Tits"
}

func maxHappinessString(i int) string {
	if i < 80 {
		return "Grumpy"
	} else if i < 95 {
		return "Pessimistic"
	} else if i < 105 {
		return "Average"
	} else if i < 120 {
		return "Optimistic"
	}
	return "Manic"
}
