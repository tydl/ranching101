package cow

import (
	"fmt"
	"gitlab.com/tydl/moomoo/util"
	"math/rand"
	"time"
)

const (
	minMaxHappiness     = 75
	maxMaxHappiness     = 150
	minMaxMilk          = 25
	maxMaxMilk          = 200
	minBaseHarvestTime  = time.Minute * 30
	maxBaseHarvestTime  = time.Hour * 2
	minBaseLoveTime     = time.Minute * 10
	maxBaseLoveTime     = time.Hour
	minBaseRecoveryTime = time.Hour * 4
	maxBaseRecoveryTime = time.Hour * 24
	minDurationPerUnit  = time.Minute * 5
	maxDurationPerUnit  = time.Minute * 30
)

func NewRandLivestock(r *rand.Rand) Livestock {
	choice := r.Intn(100)
	if choice < 45 {
		return getRandCow(r)
	} else if choice < 90 {
		return getRandBull(r)
	} else {
		return getRandFuta(r)
	}
}

func getRandCow(r *rand.Rand) Livestock {
	id := util.GetIdString()
	maxHappiness := util.UniformBetween(r, minMaxHappiness, maxMaxHappiness)
	maxMilk := util.UniformBetween(r, minMaxMilk, maxMaxMilk)
	return &basicCow{
		Ident:                id,
		CowName:              fmt.Sprintf("Cow %s", id),
		MaxHappiness:         maxHappiness,
		CurrHappiness:        maxHappiness,
		Injury:               None,
		MaxMilk:              maxMilk,
		Milk:                 maxMilk,
		BaseHarvestTime:      util.UniformBetweenDurations(r, minBaseHarvestTime, maxBaseHarvestTime),
		BaseLoveTime:         util.UniformBetweenDurations(r, minBaseLoveTime, maxBaseLoveTime),
		BaseInjuryRate:       0.05,
		BaseHappinessHarvest: -10,
		BaseHappinessLove:    50,
		BaseRecoveryTime:     util.UniformBetweenDurations(r, minBaseRecoveryTime, maxBaseRecoveryTime),
		BaseGrowthRate:       2,
		DurationPerUnit:      util.UniformBetweenDurations(r, minDurationPerUnit, maxDurationPerUnit),
		TimeToRecover:        0,
	}
}

func getRandBull(r *rand.Rand) Livestock {
	id := util.GetIdString()
	maxHappiness := util.UniformBetween(r, minMaxHappiness, maxMaxHappiness)
	maxSemen := util.UniformBetween(r, minMaxMilk, maxMaxMilk)
	return &basicBull{
		Ident:                id,
		BullName:             fmt.Sprintf("Bull %s", id),
		MaxHappiness:         maxHappiness,
		CurrHappiness:        maxHappiness,
		Injury:               None,
		MaxSemen:             maxSemen,
		Semen:                maxSemen,
		BaseHarvestTime:      util.UniformBetweenDurations(r, minBaseHarvestTime, maxBaseHarvestTime),
		BaseLoveTime:         util.UniformBetweenDurations(r, minBaseLoveTime, maxBaseLoveTime),
		BaseInjuryRate:       0.05,
		BaseHappinessHarvest: -10,
		BaseHappinessLove:    50,
		BaseRecoveryTime:     util.UniformBetweenDurations(r, minBaseRecoveryTime, maxBaseRecoveryTime),
		BaseGrowthRate:       2,
		DurationPerUnit:      util.UniformBetweenDurations(r, minDurationPerUnit, maxDurationPerUnit),
		TimeToRecover:        0,
	}
}

func getRandFuta(r *rand.Rand) Livestock {
	id := util.GetIdString()
	maxHappiness := util.UniformBetween(r, minMaxHappiness, maxMaxHappiness)
	maxSemen := util.UniformBetween(r, minMaxMilk, maxMaxMilk)
	maxMilk := util.UniformBetween(r, minMaxMilk, maxMaxMilk)
	return &basicFuta{
		Ident:                id,
		FutaName:             fmt.Sprintf("Futa %s", id),
		MaxHappiness:         maxHappiness,
		CurrHappiness:        maxHappiness,
		Injury:               None,
		MaxMilk:              maxMilk,
		Milk:                 maxMilk,
		MaxSemen:             maxSemen,
		Semen:                maxSemen,
		BaseHarvestTime:      util.UniformBetweenDurations(r, minBaseHarvestTime, maxBaseHarvestTime),
		BaseLoveTime:         util.UniformBetweenDurations(r, minBaseLoveTime, maxBaseLoveTime),
		BaseInjuryRate:       0.05,
		BaseHappinessHarvest: -10,
		BaseHappinessLove:    50,
		BaseRecoveryTime:     util.UniformBetweenDurations(r, minBaseRecoveryTime, maxBaseRecoveryTime),
		BaseGrowthRate:       2,
		DurationPerUnit:      util.UniformBetweenDurations(r, minDurationPerUnit, maxDurationPerUnit),
		TimeToRecover:        0,
	}
}
