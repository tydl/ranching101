package cow

import (
	"bytes"
	"gitlab.com/tydl/moomoo/util"
	"text/template"
)

const (
	harvestCowTemplateString = "The harvester squeaks as it slowly comes to life, " +
		"{{if ge .MaxHappiness 90}}startling the cow{{else}}but the cow seems to not care{{end}}. " +
		"It gently begins to pull on her " +
		"{{if ge .MaxMilk 175}}massive and gaping" +
		"{{else if ge .MaxMilk 125}}large" +
		"{{else if ge .MaxMilk 75}}pointed" +
		"{{else if ge .MaxMilk 50}}small" +
		"{{else}}tiny{{end}} nipples. " +
		"The cow {{if ge .Happiness 50}}coos{{else}}huffs{{end}} as the first drips " +
		"secrete from her nipples. The machine continues to pick up steam, noisily " +
		"pumping with greater intensity. The harnessed animal begins to twitch with each " +
		"hiss of the motor, sweat beading on its body. The drip from her nipples " +
		"{{if ge .Milk 175}}begins pouring out, triggering an ecstatic moan from the cow" +
		"{{else if ge .Milk 125}}turns into a steady stream. The cow lets out a blissful moo" +
		"{{else if ge .Milk 75}}becomes a small stream and the cow makes a soft sound of happiness" +
		"{{else if ge .Milk 50}}drips more quickly but the cow seems bored" +
		"{{else}}stays steady. The cow huffs in impatience{{end}}. " +
		"As the machine keeps chugging, you can see it filling with her milk. By the time the machine " +
		"begins to slow down, she is covered in sweat. " +
		"{{if ge .Happiness 100}}You notice she is also wet from the stimulation and panting with her mouth open" +
		"{{if ge .Happiness 125}} while touching herself in pleasure{{end}}." +
		"{{else if ge .Happiness 75}}You notice she is wet from the excitement." +
		"{{else if lt .Happiness 50}}You can tell she is not very happy with the ordeal.{{end}}"
)

var harvestCowTemplate = template.Must(template.New("").Parse(harvestCowTemplateString))

func getHarvestCowText(b *basicCow, milk int) string {
	var doc bytes.Buffer
	err := harvestCowTemplate.Execute(&doc, struct {
		MaxHappiness int
		MaxMilk      int
		Milk         int
		Happiness    int
	}{
		MaxHappiness: b.MaxHappiness,
		MaxMilk:      b.MaxMilk,
		Milk:         milk,
		Happiness:    b.CurrHappiness,
	})
	if err != nil {
		panic(err)
	}
	return util.WordWrap(doc.String())
}

const (
	harvestBullTemplateString = "The harvester's pump sprints to life, causing the boy bull to jump at " +
		"the new loud noise and sudden suction around his member. He " +
		"{{if ge .MaxHappiness 90}}gets flustered and tries to move in his restraints." +
		"{{else}} calms down and returns to his bored state.{{end}} " +
		"After a couple minutes, you see the bull's " +
		"{{if ge .MaxSemen 175}}massive member rise up, its width pressing against the glass pump." +
		"{{else if ge .MaxSemen 125}}large penis rise, pulling the glass pump towards his chest." +
		"{{else if ge .MaxSemen 75}}penis stand erect within the glass pump." +
		"{{else if ge .MaxSemen 50}}small penis stand in the glass pump." +
		"{{else}}tiny penis come to a point in the glass pump.{{end}} " +
		"The bull begins to thrust his hips with the sound of the pump's pulsating suction, " +
		"{{if ge .Happiness 50}}huffing a moan{{else}}snorting in anger{{end}}. " +
		"The manly bull begins to sweat as his hips continue to thrust more quickly. His penis " +
		"{{if ge .MaxSemen 175}}throbs against the glass, making you unsure if the pump will hold." +
		"{{else if ge .MaxSemen 125}}throbs and occasionally presses against the pump's glass." +
		"{{else}}pulsates and twitches within the tube.{{end}} " +
		"He thrusts faster and droplets of pre begin to emerge from his head. They are quickly " +
		"carried off by the pump. The bull extends his hip even faster, rattling his harness and the " +
		"harvester as he approaches the peak of pleasure. A loud moan heralds the " +
		"{{if ge .Semen 175}}semen flooding the harvester's tubes. So much emerges that " +
		"the glass pump enclosure begins backfilling with his cum, forming spiderlike webs as it crawls in any remaining " +
		"gaps between the pump and his gigantic throbbing member. Some starts to dribble out around the seal at his base " +
		"while the pump continues to whine under the huge load." +
		"{{else if ge .Semen 125}}flood of semen that instantly coats the inside of the pump a cream " +
		"color, the pump's motor whining louder under the sudden load." +
		"{{else if ge .Semen 75}}coming within the pump, ropes of it painting the inside" +
		"of the pump a creamy white." +
		"{{else if ge .Semen 50}}bull's orgasm, cum flying out and being sucked into the harvester." +
		"{{else}}bulls' orgasm, a small dribble emerging for the harvester to collect.{{end}} " +
		"The machine starts to turn down as it handles the last of his load. The bull is soaked in sweat and breathes loudly. " +
		"{{if ge .Happiness 100}}He smiles as he rests in his harness." +
		"{{else if ge .Happiness 75}}He rests in his harness with a look on contentment." +
		"{{else if lt .Happiness 50}}He angrily pushes and pulls against his harness as the session ends.{{end}}"
)

var harvestBullTemplate = template.Must(template.New("").Parse(harvestBullTemplateString))

func getHarvestBullText(b *basicBull, semen int) string {
	var doc bytes.Buffer
	err := harvestBullTemplate.Execute(&doc, struct {
		MaxHappiness int
		MaxSemen     int
		Semen        int
		Happiness    int
	}{
		MaxHappiness: b.MaxHappiness,
		MaxSemen:     b.MaxSemen,
		Semen:        semen,
		Happiness:    b.CurrHappiness,
	})
	if err != nil {
		panic(err)
	}
	return util.WordWrap(doc.String())
}

const (
	harvestFutaTemplateString = "The cow, tight in its harness, squirms in sudden discomfort as the " +
		"harvester churns to life. The pumps begin to gently pull at her breasts and penis in mechanical rhythm " +
		"{{if ge .MaxHappiness 90}}causing her to blush" +
		"{{else}}but she pays no attention to it{{end}}. " +
		"After a few minutes have passed, you see sweat glistening on her body as the pumps pull at her " +
		"{{if ge .MaxMilk 175}}massive, gaping" +
		"{{else if ge .MaxMilk 125}}large" +
		"{{else if ge .MaxMilk 75}}pointed" +
		"{{else if ge .MaxMilk 50}}small" +
		"{{else}}tiny{{end}} nipples and her " +
		"{{if ge .MaxSemen 175}}massive member, whose girth presses against the confines of the glass pump." +
		"{{else if ge .MaxSemen 125}}large cock, the tip of which is pulling the glass pump towards her chest." +
		"{{else if ge .MaxSemen 75}}erect cock." +
		"{{else if ge .MaxSemen 50}}small cock standing in the glass pump." +
		"{{else}}tiny cock coming to a point in the glass pump.{{end}} " +
		"The cow {{if ge .Happiness 50}}coos{{else}}huffs{{end}} as milk begins to drip " +
		"into the pump, sending her hips thrusting into the air. " +
		"{{if ge .MaxSemen 125}}The glass pump around her cock clinks against the pumps on the cow's chest " +
		"with each jerky movement of her hips.{{end}} " +
		"Beads of sweat race down her back as milk " +
		"{{if ge .Milk 175}}begins flooding the chest pump, sending both the machine and cow into a chorus of loud whining noises" +
		"{{else if ge .Milk 125}}clouds her nipples from outside view. The cow lets out a blissful moo" +
		"{{else if ge .Milk 75}}becomes a small stream. The cow makes a soft sound of happiness" +
		"{{else if ge .Milk 50}}drips more quickly" +
		"{{else}}stays in a steady drip{{end}}. " +
		"Her hips twitch faster as pre emerges from her head. Her swollen cock " +
		"{{if ge .MaxSemen 175}}ripples and swells against the glass." +
		"{{else if ge .MaxSemen 125}}occasionally presses against the pump's glass with each throb." +
		"{{else}}pulsates and twitches within the glass case.{{end}} " +
		"The pump carries off the droplets as the cow pants in her harness. " +
		"It begins to pulsate more quickly, causing her to thust even harder. She moans and moos " +
		"until it raises into a cry, and " +
		"{{if ge .Semen 175}}semen erupts into the harvester's tubes. There is so much cum that " +
		"the glass pump enclosure begins backfilling with her seed, sending a stream of cum out in the " +
		"supposedly-sealed ring around her gigantic throbbing member. The pump whines and sends off a burning smell " +
		" from the load." +
		"{{else if ge .Semen 125}}a flood of semen instantly coats the inside of the pump a cream " +
		"color, the pump's motor whining louder with the sudden assault." +
		"{{else if ge .Semen 75}}seed goes flying within the pump. Thick ropes of it paint the inside" +
		"of the pump a creamy white." +
		"{{else if ge .Semen 50}}her orgasm sends her shuddering. Cum flies out and is sucked into the harvester." +
		"{{else}}she orgasms, a small dribble emerging from her head for the harvester to collect.{{end}} " +
		"The pump begins powering down as the semen tide subsides. The futa cow pants heavily, rattling the harness " +
		"she is strapped to with each breath. " +
		"{{if ge .Happiness 100}}She still continues to drip from her quivering vagina" +
		"{{if ge .Happiness 125}} while she touches herself in pleasure{{end}}." +
		"{{else if ge .Happiness 75}}The inside of her thighs are wet from the excitement." +
		"{{else if lt .Happiness 50}}You can tell she is not very happy with the ordeal.{{end}}"
)

var harvestFutaTemplate = template.Must(template.New("").Parse(harvestFutaTemplateString))

func getHarvestFutaText(b *basicFuta, milk, semen int) string {
	var doc bytes.Buffer
	err := harvestFutaTemplate.Execute(&doc, struct {
		MaxHappiness int
		MaxSemen     int
		Semen        int
		MaxMilk      int
		Milk         int
		Happiness    int
	}{
		MaxHappiness: b.MaxHappiness,
		MaxSemen:     b.MaxSemen,
		Semen:        semen,
		MaxMilk:      b.MaxMilk,
		Milk:         milk,
		Happiness:    b.CurrHappiness,
	})
	if err != nil {
		panic(err)
	}
	return util.WordWrap(doc.String())
}
