package barn

import (
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/util"
	"math/rand"
)

const (
	loveStationDailyMaint          = 250
	minBarnSlotSize                = 4
	maxBarnSlotSize                = 20
	harvestStationMaintPerSize     = 100
	harvestStationCostPerSize      = 500
	harvestStationOccupancePerSize = 1
	minHarvestStationSize          = 1
	maxHarvestStationSize          = 3
	minHarvestStationMaxCap        = 150
	maxHarvestStationMaxCap        = 2000
	minPenSize                     = 1
	maxPenSize                     = 2
	penMaintPerSize                = 45
	penCostPerSize                 = 150
	penOccupancePerSize            = 1
	harvesterMultiplierRange       = 100
	loveStationMultiplierRange     = 150
)

func NewRandBarn(r *rand.Rand) Barn {
	slots := util.UniformBetween(r, minBarnSlotSize, maxBarnSlotSize)
	return &basicBarn{
		Name:     fmt.Sprintf("Barn %s", util.GetIdString()),
		MaxSlots: slots,
		MySlots:  make([]BarnSlotter, 0, slots),
	}
}

func NewRandBarnSlotter(r *rand.Rand) BarnSlotter {
	kind := rand.Intn(100)
	if kind < 30 {
		return &basicLoveStation{
			basicSlotter: basicSlotter{
				SlotSize:   2,
				DailyMaint: loveStationDailyMaint,
				BaseCost:   1000,
			},
			basicOccupee: basicOccupee{
				MaxCap: 2,
				Inside: make([]cow.Livestock, 0, 2),
			},
			TimeMult:      util.UniformMultiplier(r, loveStationMultiplierRange),
			HappinessMult: util.UniformMultiplier(r, loveStationMultiplierRange),
			GrowthMult:    util.UniformMultiplier(r, loveStationMultiplierRange),
		}
	} else if kind < 60 {
		size := util.UniformBetween(r, minHarvestStationSize, maxHarvestStationSize)
		return &basicHarvestStation{
			basicSlotter: basicSlotter{
				SlotSize:   size,
				DailyMaint: harvestStationMaintPerSize * size,
				BaseCost:   harvestStationCostPerSize * size,
			},
			basicOccupee: basicOccupee{
				MaxCap: size * harvestStationOccupancePerSize,
				Inside: make([]cow.Livestock, 0, size*harvestStationOccupancePerSize),
			},
			ProductMult:   util.UniformMultiplier(r, harvesterMultiplierRange),
			TimeMult:      util.UniformMultiplier(r, harvesterMultiplierRange),
			HappinessMult: util.UniformMultiplier(r, harvesterMultiplierRange),
			InjuryMult:    util.UniformMultiplier(r, harvesterMultiplierRange),
			GrowthMult:    util.UniformMultiplier(r, harvesterMultiplierRange),
			RecoveryMult:  util.UniformMultiplier(r, harvesterMultiplierRange),
			MaxCap:        util.UniformBetween(r, minHarvestStationMaxCap, maxHarvestStationMaxCap),
			CurrAmount:    0,
		}
	}
	size := util.UniformBetween(r, minPenSize, maxPenSize)
	return &basicPen{
		basicSlotter{
			SlotSize:   size,
			DailyMaint: penMaintPerSize * size,
			BaseCost:   penCostPerSize * size,
		},
		basicOccupee{
			MaxCap: size * penOccupancePerSize,
			Inside: make([]cow.Livestock, 0, size*penOccupancePerSize),
		},
	}
}
