package barn

import (
	"bytes"
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/util"
	"text/template"
)

const (
	bullMasturbateString = "The bull masturbates."
	cowMasturbateString  = "The cow masturbates."
	futaMasturbateString = "The futa cow masturbates."
	bullBullSexString    = "The two bulls have sex."
	cowBullSexString     = "The bull and cow have sex."
	futaBullSexString    = "The futa cow and bull have sex."
	cowCowSexString      = "The two cows have sex."
	futaCowSexString     = "The futa cow and cow have sex."
	futaFutaSexString    = "The two futa cows have sex."
)

var bullMasturbateTemplate = template.Must(template.New("").Parse(bullMasturbateString))
var cowMasturbateTemplate = template.Must(template.New("").Parse(cowMasturbateString))
var futaMasturbateTemplate = template.Must(template.New("").Parse(futaMasturbateString))
var bullBullSexTemplate = template.Must(template.New("").Parse(bullBullSexString))
var cowBullSexTemplate = template.Must(template.New("").Parse(cowBullSexString))
var futaBullSexTemplate = template.Must(template.New("").Parse(futaBullSexString))
var cowCowSexTemplate = template.Must(template.New("").Parse(cowCowSexString))
var futaCowSexTemplate = template.Must(template.New("").Parse(futaCowSexString))
var futaFutaSexTemplate = template.Must(template.New("").Parse(futaFutaSexString))

func getSexText(livestocks []cow.Livestock) string {
	switch len(livestocks) {
	case 1:
		return getMasturbateText(livestocks[0])
	case 2:
		return getTwoSexText(livestocks[0], livestocks[1])
	default:
		panic(len(livestocks))
	}
}

func getMasturbateText(livestock cow.Livestock) string {
	data := make(map[string]interface{}, 0)
	if isFuta(livestock) {
		addFutaData(1, livestock, data)
		return getSexTextFromTemplate(data, futaMasturbateTemplate)
	} else if isMale(livestock) {
		addMaleData(1, livestock, data)
		return getSexTextFromTemplate(data, bullMasturbateTemplate)
	} else {
		addFemaleData(1, livestock, data)
		return getSexTextFromTemplate(data, cowMasturbateTemplate)
	}
}

func getTwoSexText(first, second cow.Livestock) string {
	var numberFuta, numberMale, numberFemale = 0, 0, 0
	data := make(map[string]interface{}, 0)
	if isFuta(first) {
		numberFuta++
		addFutaData(numberFuta, first, data)
	} else if isMale(first) {
		numberMale++
		addMaleData(numberMale, first, data)
	} else {
		numberFemale++
		addFemaleData(numberFemale, first, data)
	}
	if isFuta(second) {
		numberFuta++
		addFutaData(numberFuta, second, data)
	} else if isMale(second) {
		numberMale++
		addMaleData(numberMale, second, data)
	} else {
		numberFemale++
		addFemaleData(numberFemale, second, data)
	}
	if util.Debug {
		fmt.Printf("Debug: getTwoSexText:\n\tnumberFuta=%d\n\tnumberMale=%d\n\tnumberFemale=%d\n\tdata=%s\n", numberFuta, numberMale, numberFemale, data)
	}
	if numberFuta == 2 {
		return getSexTextFromTemplate(data, futaFutaSexTemplate)
	} else if numberMale == 2 {
		return getSexTextFromTemplate(data, bullBullSexTemplate)
	} else if numberFemale == 2 {
		return getSexTextFromTemplate(data, cowCowSexTemplate)
	} else if numberFuta == 1 {
		if numberFemale == 1 {
			return getSexTextFromTemplate(data, futaCowSexTemplate)
		} else {
			return getSexTextFromTemplate(data, futaBullSexTemplate)
		}
	} else if numberMale == 1 {
		return getSexTextFromTemplate(data, cowBullSexTemplate)
	} else {
		panic("numbers don't add up")
	}
}

func addFutaData(idx int, futa cow.Livestock, data map[string]interface{}) {
	// TODO
}

func addMaleData(idx int, bull cow.Livestock, data map[string]interface{}) {
	// TODO
}

func addFemaleData(idx int, moo cow.Livestock, data map[string]interface{}) {
	// TODO
}

func getSexTextFromTemplate(data interface{}, tmpl *template.Template) string {
	var doc bytes.Buffer
	err := tmpl.Execute(&doc, data)
	if err != nil {
		panic(err)
	}
	return util.WordWrap(doc.String())
}

func isMale(livestock cow.Livestock) bool {
	_, ok := livestock.(cow.Semener)
	return ok
}

func isFemale(livestock cow.Livestock) bool {
	_, ok := livestock.(cow.Milkable)
	return ok
}

func isFuta(livestock cow.Livestock) bool {
	return isMale(livestock) && isFemale(livestock)
}
