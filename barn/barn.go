package barn

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"strings"
)

const (
	baseBarnCost = 5000
	costPerSlot  = 350
)

var _ Barn = &basicBarn{}

type basicBarn struct {
	Name     string
	MaxSlots int
	MySlots  []BarnSlotter
}

func (b *basicBarn) UnmarshalJSON(data []byte) error {
	s := struct {
		Name     string
		MaxSlots int
		MySlots  []json.RawMessage
	}{}
	err := json.Unmarshal(data, &s)
	if err != nil {
		return err
	}
	b.Name = s.Name
	b.MaxSlots = s.MaxSlots
	b.MySlots, err = UnmarshalPens(s.MySlots)
	return err
}

func (b *basicBarn) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicBarn) SetName(s string) {
	b.Name = s
}

func (b *basicBarn) BarnName() string {
	return b.Name
}

func (b *basicBarn) Slots() (used, max int) {
	max = b.MaxSlots
	for _, s := range b.MySlots {
		used += s.Size()
	}
	return
}

func (b *basicBarn) Cost() int {
	return b.MaxSlots*costPerSlot + baseBarnCost
}

func (b *basicBarn) BarnSlotters() []BarnSlotter {
	return b.MySlots
}

func (b *basicBarn) AddBarnSlotter(s BarnSlotter) {
	b.MySlots = append(b.MySlots, s)
}

func (b *basicBarn) RemoveBarnSlotter(idx int) (BarnSlotter, error) {
	if len(b.MySlots) == 0 {
		return nil, errors.New("No pens to remove")
	}
	toRet := b.MySlots[idx]
	b.MySlots = append(b.MySlots[:idx], b.MySlots[idx+1:]...)
	return toRet, nil
}

func (b *basicBarn) Status() string {
	used, max := b.Slots()
	// TODO: Later: Add livestock count
	return fmt.Sprintf("%s: Slots %d/%d", b.Name, used, max)
}

func (b *basicBarn) DetailedStatus() string {
	descs := make([]string, 0, len(b.BarnSlotters())+1)
	descs = append(descs, b.Status())
	for i, s := range b.BarnSlotters() {
		descs = append(descs, fmt.Sprintf("[%d] %s", i, s.Status()))
	}
	return strings.Join(descs, "\n")
}

func (b *basicBarn) TotalMaintenanceCost() int {
	sum := 0
	for _, s := range b.MySlots {
		sum += s.DailyMaintenanceCost()
	}
	return sum
}

func LoadBarns(barns json.RawMessage, stock map[string]cow.Livestock) ([]Barn, error) {
	var rawBarns []json.RawMessage
	err := json.Unmarshal([]byte(barns), &rawBarns)
	if err != nil {
		return nil, err
	}
	fmt.Printf("%d, %s\n", len(rawBarns), rawBarns)
	result := make([]Barn, 0, len(rawBarns))
	for _, d := range rawBarns {
		fmt.Printf("Iterate over barn: %s\n", d)
		res, err := UnmarshalBarn(d)
		if err != nil {
			return nil, err
		}
		result = append(result, res)
	}
	err = connectDots(result, stock)
	fmt.Println("Done connecting dots")
	return result, err
}

func connectDots(barns []Barn, stock map[string]cow.Livestock) error {
	for _, b := range barns {
		for _, p := range b.BarnSlotters() {
			switch v := p.(type) {
			case *basicPen:
				if err := v.loadIds(stock); err != nil {
					return err
				}
			case *basicHarvestStation:
				if err := v.loadIds(stock); err != nil {
					return err
				}
			case *basicLoveStation:
				if err := v.loadIds(stock); err != nil {
					return err
				}
			default:
				return errors.New(fmt.Sprintf("could not connect livestock for type: %T %v", p, p))
			}
		}
	}
	return nil
}

func UnmarshalBarn(b []byte) (Barn, error) {
	barn := &basicBarn{}
	err := barn.UnmarshalJSON(b)
	return barn, err
}
