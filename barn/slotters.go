package barn

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/util"
	"strings"
	"time"
)

const (
	penBaseCost                       = 1000
	overnightOccupiedHarvesterMaint   = 2500
	harvesterMaxCapCostRate           = 10
	harvesterProductMultRate          = 30
	harvesterTimeMultRate             = -20
	harvesterHappinessMultRate        = 10
	harvesterInjuryMultRate           = -10
	harvesterGrowthMultRate           = 20
	harvesterRecoveryMultRate         = -20
	overnightOccupiedLoveStationMaint = 500
	loveStationBaseCost               = 5000
	loveStationTimeMultRate           = -20
	loveStationHappinessMultRate      = 75
	loveStationGrowthMultRate         = 20
)

var _ Occupyable = &basicOccupee{}

type basicOccupee struct {
	MaxCap    int
	Inside    []cow.Livestock
	loadedIds []string
}

func (o *basicOccupee) UnmarshalJSON(b []byte) error {
	s := struct {
		MaxCap int
		Ids    []string
	}{}
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	o.loadedIds = s.Ids
	o.MaxCap = s.MaxCap
	o.Inside = make([]cow.Livestock, 0, len(s.Ids))
	fmt.Printf("loadedIds=%s\n", o.loadedIds)
	return nil
}

func (b *basicOccupee) MarshalJSON() ([]byte, error) {
	ids := make([]string, 0, len(b.Inside))
	for _, i := range b.Inside {
		ids = append(ids, i.Id())
	}
	return json.Marshal(struct {
		MaxCap int
		Ids    []string
	}{
		b.MaxCap,
		ids,
	})
}

func (b *basicOccupee) Capacity() int {
	return b.MaxCap
}

func (b *basicOccupee) Add(l cow.Livestock) {
	b.Inside = append(b.Inside, l)
}

func (b *basicOccupee) Get() []cow.Livestock {
	return b.Inside
}

func (b *basicOccupee) GetById(id string) cow.Livestock {
	for _, l := range b.Inside {
		if id == l.Id() {
			return l
		}
	}
	return nil
}

func (b *basicOccupee) Remove(id string) cow.Livestock {
	for i := 0; i < len(b.Inside); i++ {
		if b.Inside[i].Id() == id {
			toReturn := b.Inside[i]
			b.Inside = append(b.Inside[:i], b.Inside[i+1:]...)
			return toReturn
		}
	}
	return nil
}

func (b *basicOccupee) Pass(dur time.Duration) {
	for _, l := range b.Inside {
		l.Pass(dur)
	}
}

func (b *basicOccupee) loadIds(stock map[string]cow.Livestock) error {
	for _, idx := range b.loadedIds {
		l, ok := stock[idx]
		if !ok {
			return errors.New(fmt.Sprintf("Cannot find id %s", idx))
		}
		b.Add(l)
	}
	return nil
}

var _ BarnSlotter = &basicSlotter{}

type basicSlotter struct {
	SlotSize   int
	DailyMaint int
	BaseCost   int
}

func (b *basicSlotter) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicSlotter) Size() int {
	return b.SlotSize
}

func (b *basicSlotter) DailyMaintenanceCost() int {
	return b.DailyMaint
}

func (b *basicSlotter) Cost() int {
	return b.BaseCost
}

func (b *basicSlotter) Status() string {
	return fmt.Sprintf("Size: %d\n\tMaintenance: $%d/day", b.SlotSize, b.DailyMaint)
}

var _ Pen = &basicPen{}

type basicPen struct {
	basicSlotter
	basicOccupee
}

func (l *basicPen) UnmarshalJSON(b []byte) error {
	s := struct {
		Slotter json.RawMessage
		Occupee json.RawMessage
	}{}
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	err = l.basicOccupee.UnmarshalJSON([]byte(s.Occupee))
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(s.Slotter), &(l.basicSlotter))
}

func (l *basicPen) MarshalJSON() ([]byte, error) {
	slotter, err := l.basicSlotter.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawSlotter := json.RawMessage(slotter)
	occupee, err := l.basicOccupee.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawOccupee := json.RawMessage(occupee)
	s := struct {
		Slotter *json.RawMessage
		Occupee *json.RawMessage
	}{
		Slotter: &rawSlotter,
		Occupee: &rawOccupee,
	}
	return json.Marshal(s)
}

func (b *basicPen) Status() string {
	str := fmt.Sprintf("Pen:\n\t%s\n\tOccupancy: %d/%d", b.basicSlotter.Status(), len(b.Get()), b.Capacity())
	if len(b.Get()) > 0 {
		det := make([]string, 0)
		for _, l := range b.Get() {
			det = append(det, l.Id())
		}
		str = fmt.Sprintf("%s\tLivestock Ids: %s", str, strings.Join(det, ","))
	}
	return str
}

func (b *basicPen) Cost() int {
	return b.basicSlotter.Cost() + penBaseCost
}

var _ HarvestStation = &basicHarvestStation{}

type basicHarvestStation struct {
	basicSlotter
	basicOccupee
	ProductMult   float64
	TimeMult      float64
	HappinessMult float64
	InjuryMult    float64
	GrowthMult    float64
	RecoveryMult  float64
	MaxCap        int
	CurrAmount    int
}

func (l *basicHarvestStation) UnmarshalJSON(b []byte) error {
	s := struct {
		Slotter       json.RawMessage
		Occupee       json.RawMessage
		ProductMult   float64
		TimeMult      float64
		HappinessMult float64
		InjuryMult    float64
		GrowthMult    float64
		RecoveryMult  float64
		MaxCap        int
		CurrAmount    int
	}{}
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	err = l.basicOccupee.UnmarshalJSON([]byte(s.Occupee))
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(s.Slotter), &(l.basicSlotter))
	if err != nil {
		return err
	}
	l.ProductMult = s.ProductMult
	l.TimeMult = s.TimeMult
	l.HappinessMult = s.HappinessMult
	l.InjuryMult = s.InjuryMult
	l.GrowthMult = s.GrowthMult
	l.RecoveryMult = s.RecoveryMult
	l.MaxCap = s.MaxCap
	l.CurrAmount = s.CurrAmount
	return nil
}

func (l *basicHarvestStation) MarshalJSON() ([]byte, error) {
	slotter, err := l.basicSlotter.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawSlotter := json.RawMessage(slotter)
	occupee, err := l.basicOccupee.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawOccupee := json.RawMessage(occupee)
	s := struct {
		Slotter       *json.RawMessage
		Occupee       *json.RawMessage
		ProductMult   float64
		TimeMult      float64
		HappinessMult float64
		InjuryMult    float64
		GrowthMult    float64
		RecoveryMult  float64
		MaxCap        int
		CurrAmount    int
	}{
		Slotter:       &rawSlotter,
		Occupee:       &rawOccupee,
		ProductMult:   l.ProductMult,
		TimeMult:      l.TimeMult,
		HappinessMult: l.HappinessMult,
		InjuryMult:    l.InjuryMult,
		GrowthMult:    l.GrowthMult,
		RecoveryMult:  l.RecoveryMult,
		MaxCap:        l.MaxCap,
		CurrAmount:    l.CurrAmount,
	}
	return json.Marshal(s)
}

func (b *basicHarvestStation) DailyMaintenanceCost() int {
	return b.basicSlotter.DailyMaintenanceCost() + len(b.basicOccupee.Get())*overnightOccupiedHarvesterMaint
}

func (b *basicHarvestStation) Cost() int {
	return b.basicSlotter.Cost() +
		b.MaxCap*harvesterMaxCapCostRate +
		util.MultToCost(b.ProductMult, harvesterProductMultRate) +
		util.MultToCost(b.TimeMult, harvesterTimeMultRate) +
		util.MultToCost(b.HappinessMult, harvesterHappinessMultRate) +
		util.MultToCost(b.InjuryMult, harvesterInjuryMultRate) +
		util.MultToCost(b.GrowthMult, harvesterGrowthMultRate) +
		util.MultToCost(b.RecoveryMult, harvesterRecoveryMultRate)
}

func (b *basicHarvestStation) Harvest() (dur time.Duration, desc string, errs []error) {
	descs := make([]string, 0)
	for _, o := range b.basicOccupee.Get() {
		product, duration, description, err := o.Harvest(
			b.ProductMult,
			b.TimeMult,
			b.HappinessMult,
			b.InjuryMult,
			b.GrowthMult,
			b.RecoveryMult,
		)
		if duration > dur {
			dur = duration
		}
		descs = append(descs, description)
		if err != nil {
			errs = append(errs, err)
		}
		b.CurrAmount += product
		if b.CurrAmount > b.MaxCap {
			b.CurrAmount = b.MaxCap
		}
	}
	desc = strings.Join(descs, "\n\n")
	return
}

func (b *basicHarvestStation) ProductCapacity() (current, max int) {
	return b.CurrAmount, b.MaxCap
}

func (b *basicHarvestStation) Empty() int {
	toRet := b.CurrAmount
	b.CurrAmount = 0
	return toRet
}

func (b *basicHarvestStation) Status() string {
	curr, max := b.ProductCapacity()
	str := fmt.Sprintf("Size: %d\n\tMaintenance: $%d/day", b.SlotSize, b.DailyMaintenanceCost())
	str = fmt.Sprintf("Harvester:\n\t%s\n\tProduct: %d/%d\n\tOccupancy: %d/%d", str, curr, max, len(b.Get()), b.Capacity())
	if len(b.Get()) > 0 {
		det := make([]string, 0)
		for _, l := range b.Get() {
			det = append(det, l.Id())
		}
		str = fmt.Sprintf("%s\tLivestock Ids: %s", str, strings.Join(det, ","))
	}
	return str
}

var _ MedicalStation = &basicMedicalStation{}

type basicMedicalStation struct {
	basicSlotter
	basicOccupee
}

func (b *basicMedicalStation) MarshalJSON() ([]byte, error) {
	return json.Marshal(*b)
}

func (b *basicMedicalStation) DailyMaintenanceCost() int {
	// TODO: Later: impl
	return 1
}

func (b *basicMedicalStation) Cost() int {
	// TODO: Later: impl
	return 1
}

func (b *basicMedicalStation) Status() string {
	// TODO: Later: impl
	return ""
}

var _ LoveStation = &basicLoveStation{}

type basicLoveStation struct {
	basicSlotter
	basicOccupee
	TimeMult      float64
	HappinessMult float64
	GrowthMult    float64
}

func (l *basicLoveStation) UnmarshalJSON(b []byte) error {
	s := struct {
		Slotter       json.RawMessage
		Occupee       json.RawMessage
		ProductMult   float64
		TimeMult      float64
		HappinessMult float64
		GrowthMult    float64
	}{}
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	err = l.basicOccupee.UnmarshalJSON([]byte(s.Occupee))
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(s.Slotter), &(l.basicSlotter))
	if err != nil {
		return err
	}
	l.TimeMult = s.TimeMult
	l.HappinessMult = s.HappinessMult
	l.GrowthMult = s.GrowthMult
	return nil
}

func (l *basicLoveStation) MarshalJSON() ([]byte, error) {
	slotter, err := l.basicSlotter.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawSlotter := json.RawMessage(slotter)
	occupee, err := l.basicOccupee.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawOccupee := json.RawMessage(occupee)
	s := struct {
		Slotter       *json.RawMessage
		Occupee       *json.RawMessage
		ProductMult   float64
		TimeMult      float64
		HappinessMult float64
		GrowthMult    float64
	}{
		Slotter:       &rawSlotter,
		Occupee:       &rawOccupee,
		TimeMult:      l.TimeMult,
		HappinessMult: l.HappinessMult,
		GrowthMult:    l.GrowthMult,
	}
	return json.Marshal(s)
}

func (b *basicLoveStation) DailyMaintenanceCost() int {
	return b.basicSlotter.DailyMaintenanceCost() + len(b.basicOccupee.Get())*overnightOccupiedLoveStationMaint
}

func (b *basicLoveStation) Cost() int {
	return b.basicSlotter.Cost() +
		loveStationBaseCost +
		util.MultToCost(b.TimeMult, loveStationTimeMultRate) +
		util.MultToCost(b.HappinessMult, loveStationHappinessMultRate) +
		util.MultToCost(b.GrowthMult, loveStationGrowthMultRate)
}

func (b *basicLoveStation) MakeLove() (dur time.Duration, desc string, errs []error) {
	desc = getSexText(b.basicOccupee.Get())
	for _, o := range b.basicOccupee.Get() {
		duration := o.MakeLove(
			b.TimeMult,
			b.HappinessMult,
			b.GrowthMult,
		)
		if duration > dur {
			dur = duration
		}
	}
	return
}

func (b *basicLoveStation) Status() string {
	str := fmt.Sprintf("Size: %d\n\tMaintenance: $%d/day", b.SlotSize, b.DailyMaintenanceCost())
	str = fmt.Sprintf("Love Pen:\n\t%s\n\tOccupancy: %d/%d", str, len(b.Get()), b.Capacity())
	if len(b.Get()) > 0 {
		det := make([]string, 0)
		for _, l := range b.Get() {
			det = append(det, l.Id())
		}
		str = fmt.Sprintf("%s\tLivestock Ids: %s", str, strings.Join(det, ","))
	}
	return str
}

func UnmarshalPens(data []json.RawMessage) ([]BarnSlotter, error) {
	result := make([]BarnSlotter, 0, len(data))
	for _, d := range data {
		res, err := UnmarshalPen(d)
		if err != nil {
			return nil, err
		}
		result = append(result, res)
	}
	return result, nil
}

func UnmarshalPen(data json.RawMessage) (BarnSlotter, error) {
	var pen basicPen
	var harv basicHarvestStation
	var love basicLoveStation
	err := json.Unmarshal([]byte(data), &harv)
	if err != nil {
		return nil, err
	}
	if harv.ProductMult > 0 {
		return &harv, nil
	}
	err = json.Unmarshal([]byte(data), &love)
	if err != nil {
		return nil, err
	}
	if love.HappinessMult > 0 {
		return &love, nil
	}
	err = json.Unmarshal([]byte(data), &pen)
	return &pen, err
}
