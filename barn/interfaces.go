package barn

import (
	"encoding/json"
	"gitlab.com/tydl/moomoo/cow"
	"time"
)

type Statuser interface {
	Status() string
}

type Barn interface {
	Statuser
	DetailedStatus() string
	SetName(string)
	BarnName() string
	Slots() (used, max int)
	Cost() int
	BarnSlotters() []BarnSlotter
	AddBarnSlotter(BarnSlotter)
	RemoveBarnSlotter(int) (BarnSlotter, error)
	TotalMaintenanceCost() int
	json.Marshaler
}

type Occupyable interface {
	Capacity() int
	Add(cow.Livestock)
	Get() []cow.Livestock
	GetById(string) cow.Livestock
	Remove(string) cow.Livestock
	Pass(time.Duration)
}

type BarnSlotter interface {
	Statuser
	Size() int
	DailyMaintenanceCost() int
	Cost() int
}

type Pen interface {
	Occupyable
	BarnSlotter
}

type HarvestStation interface {
	Occupyable
	BarnSlotter
	Harvest() (time.Duration, string, []error)
	ProductCapacity() (current, max int)
	Empty() int
	// TODO: Later: Repair mechanics
}

type MedicalStation interface {
	Pen
}

type LoveStation interface {
	Occupyable
	BarnSlotter
	MakeLove() (time.Duration, string, []error)
}
