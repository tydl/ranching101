package market

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/util"
	"math/rand"
	"strings"
	"time"
)

const (
	maxSize         = 10
	minSize         = 3
	minAvailability = time.Hour * 24
	maxAvailability = time.Hour * 72
)

type livestockForSale struct {
	Livestock    cow.Livestock
	Availability time.Duration
}

type LivestockMarket struct {
	rand       *rand.Rand
	maxEntries int
	minEntries int
	entries    []livestockForSale
	elapsed    time.Duration
}

func NewLivestockMarket() *LivestockMarket {
	m := &LivestockMarket{
		rand:       rand.New(rand.NewSource(int64(time.Now().Nanosecond()))),
		maxEntries: maxSize,
		minEntries: minSize,
		entries:    make([]livestockForSale, 0, 10),
		elapsed:    0,
	}
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	return m
}

func (l *LivestockMarket) Load(market json.RawMessage) error {
	loader := struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []json.RawMessage
		Elapsed    time.Duration
	}{}
	err := json.Unmarshal([]byte(market), &loader)
	if err != nil {
		return err
	}
	l.rand = rand.New(rand.NewSource(loader.NextSeed))
	l.maxEntries = loader.MaxEntries
	l.minEntries = loader.MinEntries
	l.elapsed = loader.Elapsed
	return l.loadEntries(loader.Entries)
}

func (l *LivestockMarket) loadEntries(entries []json.RawMessage) error {
	l.entries = make([]livestockForSale, 0)
	for _, entry := range entries {
		e := struct {
			Livestock    json.RawMessage
			Availability time.Duration
		}{}
		err := json.Unmarshal(entry, &e)
		livestock, err := cow.UnmarshalLivestock(e.Livestock)
		if err != nil {
			return err
		}
		l.entries = append(l.entries, livestockForSale{
			Livestock:    livestock,
			Availability: e.Availability,
		})
	}
	return nil
}

func (l *LivestockMarket) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []livestockForSale
		Elapsed    time.Duration
	}{
		NextSeed:   l.rand.Int63(),
		MaxEntries: l.maxEntries,
		MinEntries: l.minEntries,
		Entries:    l.entries,
		Elapsed:    l.elapsed,
	})
}

func (l *LivestockMarket) Status() string {
	descs := make([]string, 0, len(l.entries))
	for i, b := range l.entries {
		str := fmt.Sprintf("[%d] $%d %s\n\tExpires: %s", i, b.Livestock.Cost(), b.Livestock.MarketStatus(), util.FormatDuration(b.Availability))
		descs = append(descs, str)
	}
	return strings.Join(descs, "\n")

	return ""
}

func (l *LivestockMarket) Pass(dur time.Duration) {
	for i := 0; i < len(l.entries); {
		if l.entries[i].Availability <= dur {
			l.entries = append(l.entries[:i], l.entries[i+1:]...)
		} else {
			l.entries[i].Availability = l.entries[i].Availability - dur
			i++
		}
	}
	for len(l.entries) < l.minEntries {
		l.generateEntry()
	}
	l.elapsed += dur
	for l.elapsed >= time.Hour {
		if l.rand.Intn(100) < 3 {
			l.generateEntry()
		}
		l.elapsed -= time.Hour
	}
}

func (l *LivestockMarket) Cost(idx int) (int, error) {
	if err := l.ensureExists(idx); err != nil {
		return 0, err
	}
	return l.entries[idx].Livestock.Cost(), nil
}

func (l *LivestockMarket) Buy(idx int) (cow.Livestock, error) {
	if err := l.ensureExists(idx); err != nil {
		return nil, err
	}
	toBuy := l.entries[idx]
	l.entries = append(l.entries[:idx], l.entries[idx+1:]...)
	return toBuy.Livestock, nil
}

func (l *LivestockMarket) generateEntry() {
	if len(l.entries) == l.maxEntries {
		return
	}
	l.entries = append(l.entries, l.generateNewEntry())
}

func (l *LivestockMarket) generateNewEntry() livestockForSale {
	livestock := cow.NewRandLivestock(l.rand)
	duration := util.UniformBetweenDurations(l.rand, minAvailability, maxAvailability)
	return livestockForSale{
		Livestock:    livestock,
		Availability: duration,
	}
}

func (l *LivestockMarket) ensureExists(idx int) error {
	if idx < 0 || idx >= len(l.entries) {
		return errors.New(fmt.Sprintf("Cannot buy index %d", idx))
	}
	return nil
}
