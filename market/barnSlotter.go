package market

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/barn"
	"gitlab.com/tydl/moomoo/util"
	"math/rand"
	"strings"
	"time"
)

type barnSlotterForSale struct {
	BarnSlotter  barn.BarnSlotter
	Availability time.Duration
}

type BarnSlotterMarket struct {
	rand       *rand.Rand
	maxEntries int
	minEntries int
	entries    []barnSlotterForSale
	elapsed    time.Duration
}

func NewBarnSlotterMarket() *BarnSlotterMarket {
	m := &BarnSlotterMarket{
		rand:       rand.New(rand.NewSource(int64(time.Now().Nanosecond()))),
		maxEntries: maxSize,
		minEntries: minSize,
		entries:    make([]barnSlotterForSale, 0, 10),
		elapsed:    0,
	}
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	return m
}

func (l *BarnSlotterMarket) Load(market json.RawMessage) error {
	loader := struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []json.RawMessage
		Elapsed    time.Duration
	}{}
	err := json.Unmarshal([]byte(market), &loader)
	if err != nil {
		return err
	}
	l.rand = rand.New(rand.NewSource(loader.NextSeed))
	l.maxEntries = loader.MaxEntries
	l.minEntries = loader.MinEntries
	l.elapsed = loader.Elapsed
	return l.loadEntries(loader.Entries)
}

func (l *BarnSlotterMarket) loadEntries(entries []json.RawMessage) error {
	l.entries = make([]barnSlotterForSale, 0)
	for _, entry := range entries {
		e := struct {
			BarnSlotter  json.RawMessage
			Availability time.Duration
		}{}
		err := json.Unmarshal(entry, &e)
		barnSlotter, err := barn.UnmarshalPen(e.BarnSlotter)
		if err != nil {
			return err
		}
		l.entries = append(l.entries, barnSlotterForSale{
			BarnSlotter:  barnSlotter,
			Availability: e.Availability,
		})
	}
	return nil
}

func (l *BarnSlotterMarket) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []barnSlotterForSale
		Elapsed    time.Duration
	}{
		NextSeed:   l.rand.Int63(),
		MaxEntries: l.maxEntries,
		MinEntries: l.minEntries,
		Entries:    l.entries,
		Elapsed:    l.elapsed,
	})
}

func (l *BarnSlotterMarket) Status() string {
	descs := make([]string, 0, len(l.entries))
	for i, b := range l.entries {
		str := fmt.Sprintf("[%d] $%d %s\n\tExpires: %s", i, b.BarnSlotter.Cost(), b.BarnSlotter.Status(), util.FormatDuration(b.Availability))
		descs = append(descs, str)
	}
	return strings.Join(descs, "\n")
}

func (l *BarnSlotterMarket) Pass(dur time.Duration) {
	for i := 0; i < len(l.entries); {
		if l.entries[i].Availability <= dur {
			l.entries = append(l.entries[:i], l.entries[i+1:]...)
		} else {
			l.entries[i].Availability = l.entries[i].Availability - dur
			i++
		}
	}
	for len(l.entries) < l.minEntries {
		l.generateEntry()
	}
	l.elapsed += dur
	for l.elapsed >= time.Hour {
		if l.rand.Intn(100) < 3 {
			l.generateEntry()
		}
		l.elapsed -= time.Hour
	}
}

func (l *BarnSlotterMarket) Cost(idx int) (int, error) {
	if err := l.ensureExists(idx); err != nil {
		return 0, err
	}
	return l.entries[idx].BarnSlotter.Cost(), nil
}

func (l *BarnSlotterMarket) Size(idx int) (int, error) {
	if err := l.ensureExists(idx); err != nil {
		return 0, err
	}
	return l.entries[idx].BarnSlotter.Size(), nil
}

func (l *BarnSlotterMarket) Buy(idx int) (barn.BarnSlotter, error) {
	if err := l.ensureExists(idx); err != nil {
		return nil, err
	}
	toBuy := l.entries[idx]
	l.entries = append(l.entries[:idx], l.entries[idx+1:]...)
	return toBuy.BarnSlotter, nil
}

func (l *BarnSlotterMarket) generateEntry() {
	if len(l.entries) == l.maxEntries {
		return
	}
	l.entries = append(l.entries, l.generateNewEntry())
}

func (l *BarnSlotterMarket) generateNewEntry() barnSlotterForSale {
	barnSlotter := barn.NewRandBarnSlotter(l.rand)
	duration := util.UniformBetweenDurations(l.rand, minAvailability, maxAvailability)
	return barnSlotterForSale{
		BarnSlotter:  barnSlotter,
		Availability: duration,
	}
}

func (l *BarnSlotterMarket) ensureExists(idx int) error {
	if idx < 0 || idx >= len(l.entries) {
		return errors.New(fmt.Sprintf("Cannot buy index %d", idx))
	}
	return nil
}
