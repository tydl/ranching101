package market

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/barn"
	"gitlab.com/tydl/moomoo/util"
	"math/rand"
	"strings"
	"time"
)

type barnForSale struct {
	Barn         barn.Barn
	Availability time.Duration
}

type BarnMarket struct {
	rand       *rand.Rand
	maxEntries int
	minEntries int
	entries    []barnForSale
	elapsed    time.Duration
}

func NewBarnMarket() *BarnMarket {
	m := &BarnMarket{
		rand:       rand.New(rand.NewSource(int64(time.Now().Nanosecond()))),
		maxEntries: maxSize,
		minEntries: minSize,
		entries:    make([]barnForSale, 0, 10),
		elapsed:    0,
	}
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	m.generateEntry()
	return m
}

func (l *BarnMarket) Load(market json.RawMessage) error {
	loader := struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []json.RawMessage
		Elapsed    time.Duration
	}{}
	err := json.Unmarshal([]byte(market), &loader)
	if err != nil {
		return err
	}
	l.rand = rand.New(rand.NewSource(loader.NextSeed))
	l.maxEntries = loader.MaxEntries
	l.minEntries = loader.MinEntries
	l.elapsed = loader.Elapsed
	return l.loadEntries(loader.Entries)
}

func (l *BarnMarket) loadEntries(entries []json.RawMessage) error {
	l.entries = make([]barnForSale, 0)
	for _, entry := range entries {
		e := struct {
			Barn         json.RawMessage
			Availability time.Duration
		}{}
		err := json.Unmarshal(entry, &e)
		barn, err := barn.UnmarshalBarn([]byte(e.Barn))
		if err != nil {
			return err
		}
		l.entries = append(l.entries, barnForSale{
			Barn:         barn,
			Availability: e.Availability,
		})
	}
	return nil
}

func (l *BarnMarket) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		NextSeed   int64
		MaxEntries int
		MinEntries int
		Entries    []barnForSale
		Elapsed    time.Duration
	}{
		NextSeed:   l.rand.Int63(),
		MaxEntries: l.maxEntries,
		MinEntries: l.minEntries,
		Entries:    l.entries,
		Elapsed:    l.elapsed,
	})
}

func (l *BarnMarket) Status() string {
	descs := make([]string, 0, len(l.entries))
	for i, b := range l.entries {
		str := fmt.Sprintf("[%d] $%d %s Expires: %s", i, b.Barn.Cost(), b.Barn.Status(), util.FormatDuration(b.Availability))
		descs = append(descs, str)
	}
	return strings.Join(descs, "\n")
}

func (l *BarnMarket) Pass(dur time.Duration) {
	for i := 0; i < len(l.entries); {
		if l.entries[i].Availability <= dur {
			l.entries = append(l.entries[:i], l.entries[i+1:]...)
		} else {
			l.entries[i].Availability = l.entries[i].Availability - dur
			i++
		}
	}
	for len(l.entries) < l.minEntries {
		l.generateEntry()
	}
	l.elapsed += dur
	for l.elapsed >= time.Hour {
		if l.rand.Intn(100) < 3 {
			l.generateEntry()
		}
		l.elapsed -= time.Hour
	}
}

func (l *BarnMarket) Cost(idx int) (int, error) {
	if err := l.ensureExists(idx); err != nil {
		return 0, err
	}
	return l.entries[idx].Barn.Cost(), nil
}

func (l *BarnMarket) Buy(idx int) (barn.Barn, error) {
	if err := l.ensureExists(idx); err != nil {
		return nil, err
	}
	toBuy := l.entries[idx]
	l.entries = append(l.entries[:idx], l.entries[idx+1:]...)
	return toBuy.Barn, nil
}

func (l *BarnMarket) generateEntry() {
	if len(l.entries) == l.maxEntries {
		return
	}
	l.entries = append(l.entries, l.generateNewEntry())
}

func (l *BarnMarket) generateNewEntry() barnForSale {
	barn := barn.NewRandBarn(l.rand)
	duration := util.UniformBetweenDurations(l.rand, minAvailability, maxAvailability)
	return barnForSale{
		Barn:         barn,
		Availability: duration,
	}
}

func (l *BarnMarket) ensureExists(idx int) error {
	if idx < 0 || idx >= len(l.entries) {
		return errors.New(fmt.Sprintf("Cannot buy index %d", idx))
	}
	return nil
}
