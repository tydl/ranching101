package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/tydl/moomoo/barn"
	"gitlab.com/tydl/moomoo/cow"
	"gitlab.com/tydl/moomoo/market"
	"strconv"
	"strings"
	"time"
)

const (
	pricePerUnit          = 15
	sellTime              = time.Minute * 10
	moveLivestockTime     = time.Minute * 30
	purchaseLivestockTime = time.Hour
	buyBarnTime           = time.Hour * 3
	buyBarnSlotTime       = time.Minute * 30
	removeBarnSlotTime    = time.Hour * 2
)

type Game struct {
	current           time.Time
	barns             []barn.Barn
	stock             map[string]cow.Livestock
	money             int
	livestockMarket   *market.LivestockMarket
	barnMarket        *market.BarnMarket
	barnSlotterMarket *market.BarnSlotterMarket
}

func (g *Game) UnmarshalJSON(b []byte) error {
	m := make(map[string]json.RawMessage, 0)
	err := json.Unmarshal(b, &m)
	if err != nil {
		return err
	}
	g.current, err = time.Parse(time.RFC3339, strings.Trim(string(m["current"]), "\""))
	if err != nil {
		return err
	}
	g.stock, err = cow.LoadStock(m["stock"])
	if err != nil {
		return err
	}
	g.barns, err = barn.LoadBarns(m["barns"], g.stock)
	if err != nil {
		return err
	}
	var money int64
	money, err = strconv.ParseInt(string(m["money"]), 10, 32)
	g.money = int(money)
	if err != nil {
		return err
	}
	if err = g.livestockMarket.Load(m["livestockMarket"]); err != nil {
		return err
	}
	if err = g.barnMarket.Load(m["barnMarket"]); err != nil {
		return err
	}
	if err = g.barnSlotterMarket.Load(m["penMarket"]); err != nil {
		return err
	}
	return nil

}

func (g *Game) MarshalJSON() ([]byte, error) {
	save := make(map[string]interface{})
	save["current"] = g.current.Format(time.RFC3339)
	barns, err := json.Marshal(g.barns)
	if err != nil {
		return nil, err
	}
	rawBarns := json.RawMessage(barns)
	save["barns"] = &rawBarns
	stock := make(map[string]interface{}, len(g.stock))
	for k, v := range g.stock {
		s, err := v.MarshalJSON()
		if err != nil {
			return nil, err
		}
		rawStock := json.RawMessage(s)
		stock[k] = &rawStock
	}
	save["stock"] = stock
	save["money"] = g.money
	liveMarket, err := g.livestockMarket.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawLiveMarket := json.RawMessage(liveMarket)
	save["livestockMarket"] = &rawLiveMarket
	barnMarket, err := g.barnMarket.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawBarnMarket := json.RawMessage(barnMarket)
	save["barnMarket"] = &rawBarnMarket
	penMarket, err := g.barnSlotterMarket.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawPenMarket := json.RawMessage(penMarket)
	save["penMarket"] = &rawPenMarket
	return json.Marshal(save)
}

func (g *Game) MakeLove(barnIdx, slotIdx int) (string, error, []error) {
	if err := g.checkTime(); err != nil {
		return "", err, nil
	}
	if err := g.ensureSafeBarnPenIdx(barnIdx, slotIdx); err != nil {
		return "", err, nil
	}
	loveStation, ok := g.barns[barnIdx].BarnSlotters()[slotIdx].(barn.LoveStation)
	if !ok {
		return "", errors.New("Not a love pen"), nil
	}
	dur, desc, errs := loveStation.MakeLove()
	g.moveTime(dur)
	return desc, nil, errs
}

func (g *Game) Sell(barnIdx, slotIdx int) (int, error) {
	if err := g.checkTime(); err != nil {
		return 0, err
	}
	if err := g.ensureSafeBarnPenIdx(barnIdx, slotIdx); err != nil {
		return 0, err
	}
	harvestStation, ok := g.barns[barnIdx].BarnSlotters()[slotIdx].(barn.HarvestStation)
	if !ok {
		return 0, errors.New("Not a harvest pen")
	}
	current := harvestStation.Empty()
	profit := current * pricePerUnit
	g.profit(profit)
	g.moveTime(sellTime)
	return profit, nil
}

func (g *Game) Harvest(barnIdx, slotIdx int) (string, error, []error) {
	if err := g.checkTime(); err != nil {
		return "", err, nil
	}
	if err := g.ensureSafeBarnPenIdx(barnIdx, slotIdx); err != nil {
		return "", err, nil
	}
	harvestStation, ok := g.barns[barnIdx].BarnSlotters()[slotIdx].(barn.HarvestStation)
	if !ok {
		return "", errors.New("Not a harvest pen"), nil
	}
	dur, desc, errs := harvestStation.Harvest()
	g.moveTime(dur)
	return desc, nil, errs
}

func (g *Game) BuyLivestock(marketIdx, toBarnIdx, toSlotIdx int) error {
	if err := g.checkTime(); err != nil {
		return err
	}
	if toBarnIdx < 0 && toSlotIdx < 0 {
		toBarnIdx, toSlotIdx = g.findEmptySpotInBarn()
	} else if toSlotIdx < 0 {
		toSlotIdx = g.findPenInBarnWithEmptySpot(toBarnIdx)
	}
	if err := g.ensureSafeBarnPenIdx(toBarnIdx, toSlotIdx); err != nil {
		return err
	}
	occupyableTo, ok := g.barns[toBarnIdx].BarnSlotters()[toSlotIdx].(barn.Occupyable)
	if !ok {
		return errors.New("Not an occupiable pen")
	}
	if capacity := occupyableTo.Capacity(); capacity < len(occupyableTo.Get())+1 {
		return errors.New("Not enough room in pen")
	}
	if cost, err := g.livestockMarket.Cost(marketIdx); err != nil {
		return err
	} else if err := g.purchase(cost); err != nil {
		return err
	} else if l, err := g.livestockMarket.Buy(marketIdx); err != nil {
		g.profit(cost)
		return err
	} else {
		occupyableTo.Add(l)
		g.stock[l.Id()] = l
		g.moveTime(purchaseLivestockTime)
		return nil
	}
}

func (g *Game) MoveLivestock(id string, toBarnIdx, toSlotIdx int) error {
	if err := g.checkTime(); err != nil {
		return err
	}
	if err := g.ensureSafeBarnPenIdx(toBarnIdx, toSlotIdx); err != nil {
		return err
	}
	fromBarnIdx, fromSlotIdx, err := g.findLivestockLocation(id)
	if err != nil {
		return err
	}
	occupyableFrom, ok := g.barns[fromBarnIdx].BarnSlotters()[fromSlotIdx].(barn.Occupyable)
	if !ok {
		return errors.New("From pen is not occupiable")
	}
	occupyableTo, ok := g.barns[toBarnIdx].BarnSlotters()[toSlotIdx].(barn.Occupyable)
	if !ok {
		return errors.New("Not an occupiable pen")
	}
	if occupyableFrom.GetById(id) == nil {
		return errors.New("No livestock to move")
	}
	if capacity := occupyableTo.Capacity(); capacity < len(occupyableTo.Get())+1 {
		return errors.New("Not enough room in pen")
	}
	occupyableTo.Add(occupyableFrom.Remove(id))
	g.moveTime(moveLivestockTime)
	fmt.Printf("Moved %s from %s (Pen #%d) to %s (Pen #%d)\n", g.stock[id].Name(), g.barns[fromBarnIdx].BarnName(), fromSlotIdx, g.barns[toBarnIdx].BarnName(), toSlotIdx)
	return nil
}

func (g *Game) RemoveBarnSlotter(barnIdx, slotIdx int) error {
	if err := g.checkTime(); err != nil {
		return err
	}
	if err := g.ensureSafeBarnPenIdx(barnIdx, slotIdx); err != nil {
		return err
	}
	switch o := g.barns[barnIdx].BarnSlotters()[slotIdx].(type) {
	case barn.Occupyable:
		if len(o.Get()) > 0 {
			return errors.New("Livestock still in pen")
		}
	}
	_, err := g.barns[barnIdx].RemoveBarnSlotter(slotIdx)
	g.moveTime(removeBarnSlotTime)
	return err
}

func (g *Game) BuyBarnSlotter(marketIdx, idx int) error {
	if err := g.checkTime(); err != nil {
		return err
	}
	if idx < 0 {
		idx = g.findBarnWithEmptySlot()
	}
	if err := g.ensureSafeBarnIdx(idx); err != nil {
		return err
	}
	if size, err := g.barnSlotterMarket.Size(marketIdx); err != nil {
		return err
	} else if used, max := g.barns[idx].Slots(); used+size > max {
		return errors.New("Not enough room in barn for pen")
	} else if cost, err := g.barnSlotterMarket.Cost(marketIdx); err != nil {
		return err
	} else if err := g.purchase(cost); err != nil {
		return err
	} else if s, err := g.barnSlotterMarket.Buy(marketIdx); err != nil {
		g.profit(cost)
		return err
	} else {
		g.barns[idx].AddBarnSlotter(s)
		g.moveTime(buyBarnSlotTime)
		return nil
	}
}

func (g *Game) BuyBarn(marketIdx int) error {
	if err := g.checkTime(); err != nil {
		return err
	}
	if cost, err := g.barnMarket.Cost(marketIdx); err != nil {
		return err
	} else if err := g.purchase(cost); err != nil {
		return err
	} else if m, err := g.barnMarket.Buy(marketIdx); err != nil {
		g.profit(cost)
		return err
	} else {
		g.barns = append(g.barns, m)
		g.moveTime(buyBarnTime)
		return nil
	}
}

func (g *Game) NameBarn(name string, idx int) error {
	if err := g.ensureSafeBarnIdx(idx); err != nil {
		return err
	}
	g.barns[idx].SetName(name)
	return nil
}

func (g *Game) PassDay() (bool, string) {
	nextDay := g.getNextDayBeginTime()
	durPassed := nextDay.Sub(g.current)
	g.moveTime(durPassed)
	maintenance := g.getTotalMaintenanceCost()
	g.money -= maintenance
	return g.money < 0, fmt.Sprintf("The next day comes. You pay $%d in maintenance costs", maintenance)
}

func (g *Game) GetOverallStatus() string {
	descs := make([]string, 0, len(g.barns)+1)
	descs = append(descs, "Barns:")
	for i, b := range g.barns {
		descs = append(descs, fmt.Sprintf("[%d] %s", i, b.Status()))
	}
	return strings.Join(descs, "\n")
}

func (g *Game) GetBarnStatus(idx int) (string, error) {
	if err := g.ensureSafeBarnIdx(idx); err != nil {
		return "", err
	}
	return g.barns[idx].DetailedStatus(), nil
}

func (g *Game) GetLivestockStatus(idx int) (string, error) {
	strIdx := fmt.Sprintf("%d", idx)
	if err := g.ensureLivestockIdx(strIdx); err != nil {
		return "", err
	}
	return g.stock[strIdx].Status(), nil
}

func (g *Game) GetSlotStatus(idx, slotIdx int) (string, error) {
	if err := g.ensureSafeBarnPenIdx(idx, slotIdx); err != nil {
		return "", err
	}
	return g.barns[idx].BarnSlotters()[slotIdx].Status(), nil
}

func (g *Game) GetBarnMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s", g.GetPromptText(), g.barnMarket.Status())
}

func (g *Game) GetLivestockMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s", g.GetPromptText(), g.livestockMarket.Status())
}

func (g *Game) GetPenMarketDescription() string {
	return fmt.Sprintf("%s\n\n%s", g.GetPromptText(), g.barnSlotterMarket.Status())
}

func (g *Game) GetPromptText() string {
	barnUsed := 0
	barnTotal := 0
	for _, b := range g.barns {
		used, max := b.Slots()
		barnUsed += used
		barnTotal += max
	}
	return fmt.Sprintf("Date: %s\nMoney: $%d\nMaintenance: $%d/day\nBarn Space: %d/%d\n# Livestock: %d",
		g.current.Format("January _2 15:04 2006"),
		g.money,
		g.getTotalMaintenanceCost(),
		barnUsed, barnTotal, len(g.stock))
}

func (g *Game) ensureSafeBarnIdx(barnIdx int) error {
	if barnIdx < 0 || barnIdx >= len(g.barns) {
		return errors.New(fmt.Sprintf("Barn id %d is invalid", barnIdx))
	}
	return nil
}

func (g *Game) ensureSafeBarnPenIdx(barnIdx, penIdx int) error {
	err := g.ensureSafeBarnIdx(barnIdx)
	if err != nil {
		return err
	}
	if penIdx < 0 || penIdx >= len(g.barns[barnIdx].BarnSlotters()) {
		return errors.New(fmt.Sprintf("Pen id %d is invalid", penIdx))
	}
	return nil
}

func (g *Game) ensureLivestockIdx(idx string) error {
	if _, ok := g.stock[idx]; !ok {
		return errors.New(fmt.Sprintf("Livestock id %s is invalid", idx))
	}
	return nil
}

func (g *Game) findLivestockLocation(id string) (int, int, error) {
	for barnIdx, b := range g.barns {
		for slotIdx, s := range b.BarnSlotters() {
			switch o := s.(type) {
			case barn.Occupyable:
				for _, l := range o.Get() {
					if l.Id() == id {
						return barnIdx, slotIdx, nil
					}
				}
			}
		}
	}
	return 0, 0, errors.New("Could not find livestock with that id")
}

func (g *Game) getTotalMaintenanceCost() int {
	maintenance := 0
	for _, b := range g.barns {
		maintenance += b.TotalMaintenanceCost()
	}
	return maintenance
}

func (g *Game) purchase(cost int) error {
	if g.money < cost {
		return errors.New("Not enough money")
	}
	g.money -= cost
	return nil
}

func (g *Game) profit(amt int) {
	g.money += amt
}

func (g *Game) moveTime(d time.Duration) {
	g.current = g.current.Add(d)
	for _, b := range g.barns {
		for _, s := range b.BarnSlotters() {
			switch p := s.(type) {
			case barn.Pen:
				p.Pass(d)
			}
		}
	}
	g.livestockMarket.Pass(d)
	g.barnMarket.Pass(d)
	g.barnSlotterMarket.Pass(d)
}

func (g *Game) checkTime() error {
	if g.current.After(g.getThisDayEndTime()) {
		return errors.New("Too late at night, it's time to wank and sleep")
	}
	return nil
}

func (g *Game) getNextDayBeginTime() time.Time {
	return time.Date(
		g.current.Year(),
		g.current.Month(),
		g.current.Day()+1,
		8,
		0,
		0,
		0,
		g.current.Location(),
	)
}

func (g *Game) getThisDayEndTime() time.Time {
	return time.Date(
		g.current.Year(),
		g.current.Month(),
		g.current.Day(),
		20,
		0,
		0,
		0,
		g.current.Location(),
	)
}

func (g *Game) findBarnWithEmptySlot() int {
	for i := 0; i < len(g.barns); i++ {
		used, max := g.barns[i].Slots()
		if used != max {
			return i
		}
	}
	return -1
}

func (g *Game) findEmptySpotInBarn() (int, int) {
	for idx := range g.barns {
		if penIdx := g.findPenInBarnWithEmptySpot(idx); penIdx >= 0 {
			return idx, penIdx
		}
	}
	return -1, -1
}

func (g *Game) findPenInBarnWithEmptySpot(barnIdx int) int {
	if barnIdx < 0 || barnIdx >= len(g.barns) {
		return -1
	}
	for i := 0; i < len(g.barns[barnIdx].BarnSlotters()); i++ {
		slotter := g.barns[barnIdx].BarnSlotters()[i]
		switch v := slotter.(type) {
		case barn.Occupyable:
			if len(v.Get()) < v.Capacity() {
				return i
			}
		}
	}
	return -1
}
